webpackJsonp([26],{

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BtcWalletviewPageModule", function() { return BtcWalletviewPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__btc_walletview__ = __webpack_require__(325);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BtcWalletviewPageModule = /** @class */ (function () {
    function BtcWalletviewPageModule() {
    }
    BtcWalletviewPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__btc_walletview__["a" /* BtcWalletviewPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__btc_walletview__["a" /* BtcWalletviewPage */]),
            ],
        })
    ], BtcWalletviewPageModule);
    return BtcWalletviewPageModule;
}());

//# sourceMappingURL=btc-walletview.module.js.map

/***/ }),

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtcWalletviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BtcWalletviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BtcWalletviewPage = /** @class */ (function () {
    function BtcWalletviewPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BtcWalletviewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BtcWalletviewPage');
    };
    BtcWalletviewPage.prototype.back = function () {
        this.navCtrl.setRoot('BtcYourprivatekeyPage');
    };
    BtcWalletviewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-btc-walletview',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-walletview/btc-walletview.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-row>\n      <ion-col col-1>\n        <div class="close-btn" (click)=back()>X</div>\n      </ion-col>\n      <ion-col col-9>\n        <ion-title text-center>Wallet Name</ion-title>\n      </ion-col>\n      <ion-col col-2 style="margin:0px; padding:0px;">\n          <img class="img" width="40px" height="40px" src="./assets/imgs/bitcoincash_logo.png">\n      </ion-col>\n    </ion-row>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding class="Background">\n  <ion-row class="wallet-table">\n\n    <ion-col col-3>\n      Address :\n    </ion-col>\n    <ion-col col-9>\n        0124403187852455666\n    </ion-col>\n  </ion-row>\n  <ion-row class="wallet-table">\n\n    <ion-col col-3>\n      Balance :\n    </ion-col>\n    <ion-col col-9>\n      0.0\n    </ion-col>\n  </ion-row>\n  <ion-row class="wallet-table">\n\n    <ion-col col-2>\n      EUR :\n    </ion-col>\n    <ion-col col-10 text-left>\n      0.00\n    </ion-col>\n  </ion-row>\n  <ion-row class="wallet-table">\n\n    <ion-col col-5>\n      Transactions :\n    </ion-col>\n    <ion-col col-7 text-left>\n      0\n    </ion-col>\n  </ion-row>\n  <ion-row class="wallet-table">\n\n    <ion-col col-4>\n      Private Key:\n    </ion-col>\n    <ion-col col-8>\n      0124403187852455666\n    </ion-col>\n  </ion-row>\n\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-walletview/btc-walletview.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]) === "function" && _b || Object])
    ], BtcWalletviewPage);
    return BtcWalletviewPage;
    var _a, _b;
}());

//# sourceMappingURL=btc-walletview.js.map

/***/ })

});
//# sourceMappingURL=26.js.map