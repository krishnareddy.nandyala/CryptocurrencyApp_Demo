webpackJsonp([17],{

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EthImportwalletfromjsonfilePageModule", function() { return EthImportwalletfromjsonfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__eth_importwalletfromjsonfile__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EthImportwalletfromjsonfilePageModule = /** @class */ (function () {
    function EthImportwalletfromjsonfilePageModule() {
    }
    EthImportwalletfromjsonfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__eth_importwalletfromjsonfile__["a" /* EthImportwalletfromjsonfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__eth_importwalletfromjsonfile__["a" /* EthImportwalletfromjsonfilePage */]),
            ],
        })
    ], EthImportwalletfromjsonfilePageModule);
    return EthImportwalletfromjsonfilePageModule;
}());

//# sourceMappingURL=eth-importwalletfromjsonfile.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EthImportwalletfromjsonfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the EthImportwalletfromjsonfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EthImportwalletfromjsonfilePage = /** @class */ (function () {
    function EthImportwalletfromjsonfilePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EthImportwalletfromjsonfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EthImportwalletfromjsonfilePage');
    };
    EthImportwalletfromjsonfilePage.prototype.back = function () {
        this.navCtrl.setRoot('EthWalletCreatePage');
    };
    EthImportwalletfromjsonfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-eth-importwalletfromjsonfile',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/eth-importwalletfromjsonfile/eth-importwalletfromjsonfile.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-row>\n      <ion-col col-1>\n        <div class="close-btn" (click)=back()>X</div>\n      </ion-col>\n      <ion-col col-9>\n        <ion-title text-center>Import Wallet From JSON</ion-title>\n      </ion-col>\n      <ion-col col-2 style="margin:0px; padding:0px;">\n          <img class="img" width="40px" height="40px" src="./assets/imgs/etherium_logo.png">\n      </ion-col>\n    </ion-row>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="Background">\n  <ion-row style="border: 1px solid lightslategray; padding:15px 10px; margin:10px 15px; color: #fff;">\n    <ion-col col-3 text-left>\n      <span style="font-size: 18px; padding-right: 5px;">Name</span>\n    </ion-col>\n    <ion-col col-9 style="margin:5px 0 0 0; padding:0;font-size: 16px;">\n      <input style="background:none; border:none;" type="text" placeholder="(Name of new Wallet)">}\n    </ion-col>\n  </ion-row>\n  <ion-row style="border: 1px solid lightslategray; padding:15px 10px; margin:10px 15px; color: #fff;">\n    <ion-col col-3 text-left>\n      <span style="font-size: 18px; padding-right: 5px;">File</span>\n    </ion-col>\n    <ion-col col-9 style="margin:5px 0 0 0; padding:0;font-size: 16px;">\n      <div style="background:none; border:none;" alt=""></div>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-12 padding-left padding-right>\n      <button block ion-button (click)="importWallet()">Open File</button>\n    </ion-col>\n  </ion-row>\n  <ion-row style="border: 1px solid lightslategray; padding:15px 10px; margin:10px 15px; color: #fff;">\n    <ion-col col-4 text-left>\n      <span style="font-size: 16px; padding-right: 5px;">Password</span>\n    </ion-col>\n    <ion-col col-8 style="margin:5px 0 0 0; padding:0;font-size: 16px;">\n      <input style="background:none; border:none;" type="password" placeholder="(Password JSON file)">\n    </ion-col>\n  </ion-row>\n</ion-content>\n\n<ion-footer>\n  <ion-row>\n    <ion-col col-12 padding-left padding-right>\n      <button block ion-button (click)="importWallet()">Import Wallet</button>\n    </ion-col>\n  </ion-row>\n</ion-footer>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/eth-importwalletfromjsonfile/eth-importwalletfromjsonfile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], EthImportwalletfromjsonfilePage);
    return EthImportwalletfromjsonfilePage;
}());

//# sourceMappingURL=eth-importwalletfromjsonfile.js.map

/***/ })

});
//# sourceMappingURL=17.js.map