webpackJsonp([34],{

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BtcAddwalletfromprivatekeyPageModule", function() { return BtcAddwalletfromprivatekeyPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__btc_addwalletfromprivatekey__ = __webpack_require__(317);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BtcAddwalletfromprivatekeyPageModule = /** @class */ (function () {
    function BtcAddwalletfromprivatekeyPageModule() {
    }
    BtcAddwalletfromprivatekeyPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__btc_addwalletfromprivatekey__["a" /* BtcAddwalletfromprivatekeyPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__btc_addwalletfromprivatekey__["a" /* BtcAddwalletfromprivatekeyPage */]),
            ],
        })
    ], BtcAddwalletfromprivatekeyPageModule);
    return BtcAddwalletfromprivatekeyPageModule;
}());

//# sourceMappingURL=btc-addwalletfromprivatekey.module.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtcAddwalletfromprivatekeyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BtcAddwalletfromprivatekeyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BtcAddwalletfromprivatekeyPage = /** @class */ (function () {
    function BtcAddwalletfromprivatekeyPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BtcAddwalletfromprivatekeyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BtcAddwalletfromprivatekeyPage');
    };
    BtcAddwalletfromprivatekeyPage.prototype.back = function () {
        this.navCtrl.setRoot('BtcWalletCreatePage');
    };
    BtcAddwalletfromprivatekeyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-btc-addwalletfromprivatekey',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-addwalletfromprivatekey/btc-addwalletfromprivatekey.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-row>\n      <ion-col col-1>\n        <div class="close-btn" (click)=back()>X</div>\n      </ion-col>\n      <ion-col col-9>\n        <ion-title text-center>Add Wallet from Private Key</ion-title>\n      </ion-col>\n      <ion-col col-2 style="margin:0px; padding:0px;">\n          <img class="img" width="40px" height="40px" src="./assets/imgs/bitcoincash_logo.png">\n      </ion-col>\n    </ion-row>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="Background">\n  <ion-row style="border: 1px solid lightslategray; padding:15px 10px; margin:10px 15px; color: #fff;">\n      <ion-col col-3 text-left>\n        <span style="font-size: 16px; padding-right: 5px;">Name</span>\n      </ion-col>\n      <ion-col col-9 style="margin:5px 0 0 0; padding:0;font-size: 16px;">\n        <input style="background:none; border:none;" type="text" placeholder="(Name of new Wallet)">\n        </ion-col>\n    </ion-row>\n    <ion-row style="border: 1px solid lightslategray; padding:15px 10px; margin:10px 15px; color: #fff;">\n        <ion-col col-5 text-left>\n          <span style="font-size: 16px; padding-right: 5px;">Private Key</span>\n        </ion-col>\n        <ion-col col-7 style="margin:5px 0 0 0; padding:0;font-size: 16px; text-align: left;">\n          <input style="background:none; border:none; width: 96%;" type="text" placeholder="(Private Key)">\n          </ion-col>\n      </ion-row>\n</ion-content>\n\n<ion-footer>\n<ion-row>\n  <ion-col col-12 padding-left padding-right>\n      <button block ion-button (click)="addWallet()">Add Wallet</button>\n  </ion-col>\n</ion-row>\n</ion-footer>\n'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-addwalletfromprivatekey/btc-addwalletfromprivatekey.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]) === "function" && _b || Object])
    ], BtcAddwalletfromprivatekeyPage);
    return BtcAddwalletfromprivatekeyPage;
    var _a, _b;
}());

//# sourceMappingURL=btc-addwalletfromprivatekey.js.map

/***/ })

});
//# sourceMappingURL=34.js.map