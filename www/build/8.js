webpackJsonp([8],{

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestorePageModule", function() { return RestorePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__restore__ = __webpack_require__(364);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RestorePageModule = /** @class */ (function () {
    function RestorePageModule() {
    }
    RestorePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__restore__["a" /* RestorePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__restore__["a" /* RestorePage */]),
            ],
        })
    ], RestorePageModule);
    return RestorePageModule;
}());

//# sourceMappingURL=restore.module.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestorePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RestorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RestorePage = /** @class */ (function () {
    function RestorePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RestorePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RestorePage');
    };
    RestorePage.prototype.getPrivateKey = function () {
        this.navCtrl.setRoot('PrivatekeyRestorePage');
    };
    RestorePage.prototype.getUtcFile = function () {
        this.navCtrl.setRoot('UtcFileRestorePage');
    };
    RestorePage.prototype.back = function () {
        this.navCtrl.setRoot('HomePage');
    };
    RestorePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-restore',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/restore/restore.html"*/'<ion-content class="Background">\n  <div class="close-btn" (click)=back()>X</div>\n\n  <ion-row>\n    <ion-col class="info">\n      TO Restore your wallet you need your wallet\n      <br> private key or UTC file. Don\'t worry your\n      <br> sencetive informations are safe.\n    </ion-col>\n  </ion-row>\n  <ion-row style="margin-top: 70px">\n    <ion-col style="margin: 20PX 25px">\n      <button ion-button block (click)="getPrivateKey()">From Private Key</button>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col style="margin: 10PX 25px">\n      <button block ion-button (click)="getUtcFile()">From UTC FILE</button>\n    </ion-col>\n  </ion-row>\n\n\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/restore/restore.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], RestorePage);
    return RestorePage;
}());

//# sourceMappingURL=restore.js.map

/***/ })

});
//# sourceMappingURL=8.js.map