webpackJsonp([32],{

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BtcHomePageModule", function() { return BtcHomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__btc_home__ = __webpack_require__(319);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BtcHomePageModule = /** @class */ (function () {
    function BtcHomePageModule() {
    }
    BtcHomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__btc_home__["a" /* BtcHomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__btc_home__["a" /* BtcHomePage */]),
            ],
        })
    ], BtcHomePageModule);
    return BtcHomePageModule;
}());

//# sourceMappingURL=btc-home.module.js.map

/***/ }),

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtcHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BtcHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BtcHomePage = /** @class */ (function () {
    function BtcHomePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BtcHomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BtcHomePage');
    };
    BtcHomePage.prototype.forNewWllet = function () {
        this.navCtrl.setRoot('BtcTermsofservicesPage');
    };
    BtcHomePage.prototype.back = function () {
        this.navCtrl.setRoot('DashboardPage');
    };
    BtcHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-btc-home',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-home/btc-home.html"*/'<ion-content class="background">\n    <div class="close-btn" (click)=back()>X</div>\n  <ion-row>\n      <ion-col>\n       <img class="img" src="./assets/imgs/bitcoincash_logo.png">\n      </ion-col>\n    </ion-row>\n\n  <ion-row style="margin-top: 40px">\n    <ion-col class="buttons">\n      <button ion-button (click)="forNewWllet()">Create a New Wallet</button>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col class="or">\n        <span >or</span>\n    </ion-col>\n  </ion-row>\n  \n  <ion-row>\n    <ion-col class="buttons">\n      <button ion-button (click)="restore()">Restore</button>\n    </ion-col>\n  </ion-row>\n</ion-content>\n\n'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-home/btc-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], BtcHomePage);
    return BtcHomePage;
}());

//# sourceMappingURL=btc-home.js.map

/***/ })

});
//# sourceMappingURL=32.js.map