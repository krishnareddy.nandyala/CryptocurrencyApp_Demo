webpackJsonp([9],{

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivatekeyRestorePageModule", function() { return PrivatekeyRestorePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__privatekey_restore__ = __webpack_require__(363);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PrivatekeyRestorePageModule = /** @class */ (function () {
    function PrivatekeyRestorePageModule() {
    }
    PrivatekeyRestorePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__privatekey_restore__["a" /* PrivatekeyRestorePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__privatekey_restore__["a" /* PrivatekeyRestorePage */]),
            ],
        })
    ], PrivatekeyRestorePageModule);
    return PrivatekeyRestorePageModule;
}());

//# sourceMappingURL=privatekey-restore.module.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrivatekeyRestorePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PrivatekeyRestorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PrivatekeyRestorePage = /** @class */ (function () {
    function PrivatekeyRestorePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PrivatekeyRestorePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PrivatekeyRestorePage');
    };
    PrivatekeyRestorePage.prototype.back = function () {
        this.navCtrl.setRoot('RestorePage');
    };
    PrivatekeyRestorePage.prototype.RestoreNow = function () {
        this.navCtrl.setRoot('EnterPrivateKeyPage');
    };
    PrivatekeyRestorePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-privatekey-restore',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/privatekey-restore/privatekey-restore.html"*/'\n<ion-content class="Background">\n    <div class="close-btn" (click)=back()>X</div>\n  \n    <ion-row>\n      <ion-col class="info">\n        We\'re going to help you fast as fast as <br>possible.\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4></ion-col>\n        <ion-col col-4>\n            <img src="./assets/imgs/restore_now.png">\n        </ion-col>\n        <ion-col col-4></ion-col>\n      </ion-row>\n  \n    \n    <ion-row>\n        <ion-col class="info1">\n          Now you need to enter your private key\n          <br>from which you want to restore your \n          <br>wallet.\n        </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col class="info1">\n            Make sure you enter your correct \n            <br>private key when you proceed to\n            <br>restore.\n          </ion-col>\n        </ion-row>\n\n   \n    <ion-row>\n      <ion-col style="margin: 18PX 25px">\n        <button block ion-button (click)="RestoreNow()">Restore Now</button>\n      </ion-col>\n    </ion-row>\n  \n  \n  </ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/privatekey-restore/privatekey-restore.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], PrivatekeyRestorePage);
    return PrivatekeyRestorePage;
}());

//# sourceMappingURL=privatekey-restore.js.map

/***/ })

});
//# sourceMappingURL=9.js.map