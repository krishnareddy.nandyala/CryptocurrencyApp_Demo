webpackJsonp([30],{

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BtcPassphrasePageModule", function() { return BtcPassphrasePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__btc_passphrase__ = __webpack_require__(321);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BtcPassphrasePageModule = /** @class */ (function () {
    function BtcPassphrasePageModule() {
    }
    BtcPassphrasePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__btc_passphrase__["a" /* BtcPassphrasePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__btc_passphrase__["a" /* BtcPassphrasePage */]),
            ],
        })
    ], BtcPassphrasePageModule);
    return BtcPassphrasePageModule;
}());

//# sourceMappingURL=btc-passphrase.module.js.map

/***/ }),

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtcPassphrasePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BtcPassphrasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BtcPassphrasePage = /** @class */ (function () {
    function BtcPassphrasePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BtcPassphrasePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BtcPassphrasePage');
    };
    BtcPassphrasePage.prototype.submit = function () {
        this.navCtrl.setRoot('BtcBackupPage');
    };
    BtcPassphrasePage.prototype.back = function () {
        this.navCtrl.setRoot('BtcTermsofservicesPage');
    };
    BtcPassphrasePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-btc-passphrase',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-passphrase/btc-passphrase.html"*/'<ion-content class="Background">\n    <div class="close-btn" (click)=back()>X</div>\n    <ion-row>\n      <ion-col col-4>\n      </ion-col>\n      <ion-col col-4>\n        <img class="img" src="./assets/imgs/bitcoincash_logo.png">\n      </ion-col>\n      <ion-col col-4>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="info">\n        Enter Passphrase this is the unique identy of\n        <br> your wallet this will be required at the time of\n        <br> transactions and restoring your wallet.\n      </ion-col>\n    </ion-row>\n  \n    <form #sub="ngForm" novalidate class="form-padding">\n      <ion-row>\n        <ion-col>\n          <ion-input required class="Passphrase" type="text" [(ngModel)]="Passphrase" name="Passphrase" placeholder="Enter Passphrase"\n            minlength=\'4\'></ion-input>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-input required class="Passphrase" type="text" [(ngModel)]="ConfirmPassphrase" name="ConfirmPassphrase" placeholder="Conform Passphrase"\n            minlength=\'4\'></ion-input>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col style="margin-top: 20PX">\n          <button ion-button [disabled]="sub.invalid" block (click)="submit()">Submit</button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <button block ion-button (click)="back()">Back</button>\n        </ion-col>\n      </ion-row>\n  \n  \n    </form>\n  </ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-passphrase/btc-passphrase.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], BtcPassphrasePage);
    return BtcPassphrasePage;
}());

//# sourceMappingURL=btc-passphrase.js.map

/***/ })

});
//# sourceMappingURL=30.js.map