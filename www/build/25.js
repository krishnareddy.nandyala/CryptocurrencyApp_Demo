webpackJsonp([25],{

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BtcYourprivatekeyPageModule", function() { return BtcYourprivatekeyPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__btc_yourprivatekey__ = __webpack_require__(326);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BtcYourprivatekeyPageModule = /** @class */ (function () {
    function BtcYourprivatekeyPageModule() {
    }
    BtcYourprivatekeyPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__btc_yourprivatekey__["a" /* BtcYourprivatekeyPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__btc_yourprivatekey__["a" /* BtcYourprivatekeyPage */]),
            ],
        })
    ], BtcYourprivatekeyPageModule);
    return BtcYourprivatekeyPageModule;
}());

//# sourceMappingURL=btc-yourprivatekey.module.js.map

/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtcYourprivatekeyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BtcYourprivatekeyPage = /** @class */ (function () {
    function BtcYourprivatekeyPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BtcYourprivatekeyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BtcYourprivatekeyPage');
    };
    BtcYourprivatekeyPage.prototype.goToWallet = function () {
        this.navCtrl.setRoot('BtcWalletPage');
    };
    BtcYourprivatekeyPage.prototype.back = function () {
        this.navCtrl.setRoot('BtcBackupPage');
    };
    BtcYourprivatekeyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-btc-yourprivatekey',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-yourprivatekey/btc-yourprivatekey.html"*/'<ion-content class="Background">\n    <div class="close-btn" (click)=back()>X</div>\n    <ion-row>\n      <ion-col col-4>\n      </ion-col>\n      <ion-col col-4>\n        <img class="img" src="./assets/imgs/bitcoincash_logo.png">\n      </ion-col>\n      <ion-col col-4>\n      </ion-col>\n    </ion-row>\n\n\n<ion-row>\n\n    <ion-col col-12 style="color:#fff; font-size: 20px; text-align: center; margin-top:10px;">Your Private Key:</ion-col>\n\n</ion-row>\n<ion-row>\n    <ion-col col-1>\n    </ion-col>\n    <ion-col col-10>\n        <button ion-button block (click)="printscanner()">Print Page Wallet </button>\n    </ion-col>\n    <ion-col col-1>\n    </ion-col>\n</ion-row>\n<!-- <div style="color:#fff; font-size: 15px; border: 1px solid #fff; background:none; padding:5px 10px;"><p>{{privatekey}}</p></div> -->\n<ion-row>\n    <ion-col col-1>\n    </ion-col>\n    <ion-col col-10>\n        <textarea class="textarea" disabled>{{privatekey}}</textarea>\n    </ion-col>\n    <ion-col col-1>\n    </ion-col>\n</ion-row>\n\n<ion-row>\n    <ion-col col-3></ion-col>\n    <ion-col col-6 style="margin-top:8px;">\n        <ngx-qrcode [qrc-element-type]="\'url\'" [qrc-value]="privatekey">\n        </ngx-qrcode>\n    </ion-col>\n    <ion-col col-3></ion-col>\n</ion-row>\n\n</ion-content>\n<ion-footer>\n<ion-row style="padding : 10px 0">\n    <ion-col col-1>\n    </ion-col>\n    <ion-col col-10>\n        <button ion-button block (click)="goToWallet()">GO TO WALLET </button>\n    </ion-col>\n    <ion-col col-1>\n    </ion-col>\n</ion-row>\n\n<!-- <ion-row style="border-top:1px solid #252933; margin:9px 0 5px 0">\n    <ion-col col-4 style="text-align:center">\n        <a>\n            <img class="btns-img" src="../assets/imgs/wallte.png">\n            <div class="token-name">Your Tokens</div>\n        </a>\n\n    </ion-col>\n    <ion-col col-4 style="text-align:center">\n        <a>\n            <img class="btns-img-swap" src="../assets/imgs/contacts.png">\n            <div class="token-name">Contacts</div>\n        </a>\n    </ion-col>\n    <ion-col col-4 style="text-align:center;">\n        <a>\n            <img class="btns-img" src="../assets/imgs/settings.png">\n            <div class="token-name">Settings</div>\n        </a>\n    </ion-col>\n</ion-row> -->\n</ion-footer>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-yourprivatekey/btc-yourprivatekey.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], BtcYourprivatekeyPage);
    return BtcYourprivatekeyPage;
}());

//# sourceMappingURL=btc-yourprivatekey.js.map

/***/ })

});
//# sourceMappingURL=25.js.map