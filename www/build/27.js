webpackJsonp([27],{

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BtcWalletPageModule", function() { return BtcWalletPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__btc_wallet__ = __webpack_require__(324);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BtcWalletPageModule = /** @class */ (function () {
    function BtcWalletPageModule() {
    }
    BtcWalletPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__btc_wallet__["a" /* BtcWalletPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__btc_wallet__["a" /* BtcWalletPage */]),
            ],
        })
    ], BtcWalletPageModule);
    return BtcWalletPageModule;
}());

//# sourceMappingURL=btc-wallet.module.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtcWalletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BtcWalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BtcWalletPage = /** @class */ (function () {
    function BtcWalletPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BtcWalletPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BtcWalletPage');
    };
    BtcWalletPage.prototype.back = function () {
        this.navCtrl.setRoot('BtcYourprivatekeyPage');
    };
    BtcWalletPage.prototype.addWallet = function () {
        this.navCtrl.setRoot('BtcWalletCreatePage');
    };
    BtcWalletPage.prototype.walletviewPage = function () {
        this.navCtrl.setRoot('BtcWalletviewPage');
    };
    BtcWalletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-btc-wallet',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-wallet/btc-wallet.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-row>\n        <ion-col col-1>\n          <div class="close-btn" (click)=back()>X</div>\n        </ion-col>\n        <ion-col col-9>\n          <ion-title text-center>Wallet</ion-title>\n        </ion-col>\n        <ion-col col-2 style="margin:0px; padding:0px;">\n            <img class="img" width="40px" height="40px" src="./assets/imgs/bitcoincash_logo.png">\n        </ion-col>\n      </ion-row>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content class="Background">\n    <ion-row style="border-bottom: 1px solid lightslategray;" (click)="walletviewPage()">\n      <ion-col col-2>\n        <ion-icon name="contact" width="50px" height="50px"></ion-icon>\n      </ion-col>\n      <ion-col col-8>\n        <h6>Name</h6>\n        <ion-scroll scrollX="true" style="width: 100%; height:25%; color: #fff;">\n          121544&*97&%^%$%5454115&^%7568\n        </ion-scroll>\n      </ion-col>\n      <ion-col col-2>\n        <ion-icon name="contact" width="50px" height="50px"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-row style="border-bottom: 1px solid lightslategray;">\n      <ion-col col-2>\n        <ion-icon name="contact" width="50px" height="50px"></ion-icon>\n      </ion-col>\n      <ion-col col-8>\n        <h6>Name</h6>\n        <ion-scroll scrollX="true" style="width: 100%; height:25%; color: #fff;">\n          fdasfdsafnnnnnnnnnnnnnnnnnnnnnnnnnnnnnfda fdsaf dsafdsa fsdaf dfdsnnnnnndas fdaf asdf dasfdsa fdsa\n        </ion-scroll>\n      </ion-col>\n      <ion-col col-2>\n        <ion-icon name="contact" width="50px" height="50px"></ion-icon>\n      </ion-col>\n    </ion-row>\n  </ion-content>\n  <ion-footer>\n    <ion-row>\n  \n      <ion-col col-10>\n  \n      </ion-col>\n      <ion-col col-2>\n        <ion-icon name="add-circle" (click)="addWallet()"></ion-icon>\n      </ion-col>\n    </ion-row>\n  </ion-footer>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-wallet/btc-wallet.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]) === "function" && _b || Object])
    ], BtcWalletPage);
    return BtcWalletPage;
    var _a, _b;
}());

//# sourceMappingURL=btc-wallet.js.map

/***/ })

});
//# sourceMappingURL=27.js.map