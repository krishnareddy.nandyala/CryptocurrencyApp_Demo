webpackJsonp([23],{

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmPasswordPageModule", function() { return ConfirmPasswordPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__confirm_password__ = __webpack_require__(328);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ConfirmPasswordPageModule = /** @class */ (function () {
    function ConfirmPasswordPageModule() {
    }
    ConfirmPasswordPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__confirm_password__["a" /* ConfirmPasswordPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__confirm_password__["a" /* ConfirmPasswordPage */]),
            ],
        })
    ], ConfirmPasswordPageModule);
    return ConfirmPasswordPageModule;
}());

//# sourceMappingURL=confirm-password.module.js.map

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfirmPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ConfirmPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ConfirmPasswordPage = /** @class */ (function () {
    function ConfirmPasswordPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.password = this.navParams.get("password");
    }
    ConfirmPasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ConfirmPasswordPage');
    };
    ConfirmPasswordPage.prototype.presentToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Password Mismatch',
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    ConfirmPasswordPage.prototype.submit1 = function () {
        if (this.password == this.confirmpassword) {
            this.navCtrl.setRoot('DashboardPage');
        }
        else {
            this.presentToast();
        }
    };
    ConfirmPasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-confirm-password',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/confirm-password/confirm-password.html"*/'\n<ion-content  class="Background">\n  <ion-row>\n      <ion-col>\n       <img class="img" src="./assets/imgs/ic_launcher_round-web.png">\n      </ion-col>\n    </ion-row>\n<!-- <ion-row>\n  <ion-col class="info">\n    Enter password Here\n    \n  </ion-col>\n</ion-row> -->\n\n<form  #sub="ngForm" novalidate>\n<ion-row style="margin-top: 20px">\n  <ion-col col-1></ion-col>\n  <ion-col col-10>\n    <ion-input class="confirmpassword" type="confirmpassword" required minlength="4" [(ngModel)]="confirmpassword" name="confirmpassword" placeholder="Confirm password"></ion-input>\n  </ion-col>\n  <ion-col col-1></ion-col>\n</ion-row>\n\n<ion-row>\n  <ion-col style="margin: 16px 25px!important;">\n    <button block ion-button [disabled]="sub.invalid" (click)="submit1()">submit</button>\n  </ion-col>\n</ion-row>\n</form>\n\n</ion-content>\n'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/confirm-password/confirm-password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */]])
    ], ConfirmPasswordPage);
    return ConfirmPasswordPage;
}());

//# sourceMappingURL=confirm-password.js.map

/***/ })

});
//# sourceMappingURL=23.js.map