webpackJsonp([15],{

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermsofServicePageModule", function() { return TermsofServicePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__termsof_service__ = __webpack_require__(336);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TermsofServicePageModule = /** @class */ (function () {
    function TermsofServicePageModule() {
    }
    TermsofServicePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__termsof_service__["a" /* TermsofServicePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__termsof_service__["a" /* TermsofServicePage */]),
            ],
        })
    ], TermsofServicePageModule);
    return TermsofServicePageModule;
}());

//# sourceMappingURL=termsof-service.module.js.map

/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsofServicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TermsofServicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TermsofServicePage = /** @class */ (function () {
    function TermsofServicePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.password = this.navParams.get("password");
    }
    TermsofServicePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TermsofServicePage');
    };
    TermsofServicePage.prototype.openPassphrase = function () {
        this.navCtrl.setRoot('PassphrasePage', { 'password': this.password });
    };
    TermsofServicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-termsof-service',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/eth-termsof-service/termsof-service.html"*/'\n<ion-header >\n\n  <ion-navbar >\n    <ion-title >Terms of Service</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-row>\n    <ion-col class="heading"><strong>Terms of Use</strong> </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col class="info">\n        These Terms and Conditions (the “Terms”) apply to the sale by QuickX Protocol Pvt Ltd (“Us / We”), to you (“You”), of Tokens (as defined below). Any subsequent sale, exchange or activity with the Tokens, will be subject to separate terms and conditions, as in force from time to time.\n        By purchasing Tokens from Us, You are acknowledging that You have read, understood and agreed to, these Terms. In particular, You agree that subject to clause 7.2, the Tokens are non-refundable, and we shall not be held responsible for any losses suffered by You, except as expressly set out in these Terms.\n         All Tokens are sold on an ‘as is’ basis, and to the extent permitted by law, shall be free of all warranties, including without limitation, any implied warranties relating to fitness for purpose. The value of the Tokens may fluctuate, and We will not be held responsible for any losses that You suffer as a result of such fluctuations.\n          If You are acting in the course of a business, these Terms, along with any documents referred to herein, constitute the entire agreement between Us and You in relation to your purchase of the Tokens from Us. You acknowledge that You have not relied on any statement, promise, representation, assurance or warranty made or given by or on behalf of Us which is not set out in these Terms (or the documents referred to herein), and that You shall have no claim for innocent or negligent misrepresentation or negligent misstatement, based on any statement in this Agreement. \n    </ion-col>\n  </ion-row>\n  <!-- <div style="padding: 10px 0px 10px 10px">\n               These Terms and Conditions (the “Terms”) apply to the sale by QuickX Protocol Pvt Ltd (“Us / We”), to you (“You”), of Tokens (as defined below). Any subsequent sale, exchange or activity with the Tokens, will be subject to separate terms and conditions, as in force from time to time.\n By purchasing Tokens from Us, You are acknowledging that You have read, understood and agreed to, these Terms. In particular, You agree that subject to clause 7.2, the Tokens are non-refundable, and we shall not be held responsible for any losses suffered by You, except as expressly set out in these Terms.\n  All Tokens are sold on an ‘as is’ basis, and to the extent permitted by law, shall be free of all warranties, including without limitation, any implied warranties relating to fitness for purpose. The value of the Tokens may fluctuate, and We will not be held responsible for any losses that You suffer as a result of such fluctuations.\n   If You are acting in the course of a business, these Terms, along with any documents referred to herein, constitute the entire agreement between Us and You in relation to your purchase of the Tokens from Us. You acknowledge that You have not relied on any statement, promise, representation, assurance or warranty made or given by or on behalf of Us which is not set out in these Terms (or the documents referred to herein), and that You shall have no claim for innocent or negligent misrepresentation or negligent misstatement, based on any statement in this Agreement. \n  </div> -->\n\n</ion-content>\n<ion-footer >\n    <ion-row >\n        <ion-col class="buttons">\n          <button ion-button (click)="openPassphrase()">I agree, go next</button>\n        </ion-col>\n      </ion-row>\n</ion-footer>\n'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/eth-termsof-service/termsof-service.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], TermsofServicePage);
    return TermsofServicePage;
}());

//# sourceMappingURL=termsof-service.js.map

/***/ })

});
//# sourceMappingURL=15.js.map