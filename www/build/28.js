webpackJsonp([28],{

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BtcWalletCreatePageModule", function() { return BtcWalletCreatePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__btc_wallet_create__ = __webpack_require__(323);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BtcWalletCreatePageModule = /** @class */ (function () {
    function BtcWalletCreatePageModule() {
    }
    BtcWalletCreatePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__btc_wallet_create__["a" /* BtcWalletCreatePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__btc_wallet_create__["a" /* BtcWalletCreatePage */]),
            ],
        })
    ], BtcWalletCreatePageModule);
    return BtcWalletCreatePageModule;
}());

//# sourceMappingURL=btc-wallet-create.module.js.map

/***/ }),

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtcWalletCreatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BtcWalletCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BtcWalletCreatePage = /** @class */ (function () {
    function BtcWalletCreatePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BtcWalletCreatePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BtcWalletCreatePage');
    };
    BtcWalletCreatePage.prototype.back = function () {
        this.navCtrl.setRoot('BtcWalletPage');
    };
    BtcWalletCreatePage.prototype.addWalletPrivateKey = function () {
        this.navCtrl.setRoot('BtcAddwalletfromprivatekeyPage');
    };
    BtcWalletCreatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-btc-wallet-create',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-wallet-create/btc-wallet-create.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-row>\n      <ion-col col-1>\n        <div class="close-btn" (click)=back()>X</div>\n      </ion-col>\n      <ion-col col-9>\n        <ion-title text-center>New Wallet</ion-title>\n      </ion-col>\n      <ion-col col-2 style="margin:0px; padding:0px;">\n          <img class="img" width="40px" height="40px" src="./assets/imgs/bitcoincash_logo.png">\n      </ion-col>\n    </ion-row>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="Background">\n  <ion-row style="border: 1px solid lightslategray; padding:15px 10px; margin:10px 15px; color: #fff;">\n    <ion-col col-3 text-left>\n      <span style="font-size: 18px; padding-right: 5px;">Name</span>\n    </ion-col>\n    <ion-col col-9 style="margin:5px 0 0 0; padding:0;font-size: 16px;">\n      <input style="background:none; border:none;" type="text" placeholder="(Name of new Wallet)">\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-12 padding>\n      <button block ion-button (click)="goToWallet()">Create Random Wallet</button>\n      <button block ion-button (click)="addWalletPrivateKey()">Open Wallet With Private Key</button>\n      <button block ion-button (click)="walletFromJsonfile()">Import Wallet from JSON File</button>\n    </ion-col>\n  </ion-row>\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-wallet-create/btc-wallet-create.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]) === "function" && _b || Object])
    ], BtcWalletCreatePage);
    return BtcWalletCreatePage;
    var _a, _b;
}());

//# sourceMappingURL=btc-wallet-create.js.map

/***/ })

});
//# sourceMappingURL=28.js.map