webpackJsonp([2],{

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsPageModule", function() { return TransactionsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__transactions__ = __webpack_require__(370);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TransactionsPageModule = /** @class */ (function () {
    function TransactionsPageModule() {
    }
    TransactionsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__transactions__["a" /* TransactionsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__transactions__["a" /* TransactionsPage */]),
            ],
        })
    ], TransactionsPageModule);
    return TransactionsPageModule;
}());

//# sourceMappingURL=transactions.module.js.map

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TransactionsPage = /** @class */ (function () {
    function TransactionsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TransactionsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TransactionsPage');
    };
    TransactionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-transactions',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/transactions/transactions.html"*/'<ion-header class="Background">\n  <ion-row>\n    <ion-col col-6 left>\n      <button ion-button menuToggle>\n        <img src="../../assets/imgs/menu.png">\n      </button>\n    </ion-col>\n    <ion-col col-6 style="text-align: right;">\n      <button ion-button menuToggle>\n        <img src="../../assets/imgs/sign-code.png">\n      </button>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n\n    <ion-col col-12 style="text-align:center;">\n      <img width="97%" src="../../assets/imgs/transactions.png">\n    </ion-col>\n\n  </ion-row>\n</ion-header>\n  <ion-content class="Background">\n  <div>\n    <ion-card>\n      <ion-row>\n        <ion-col col-2>\n          <img src="../../assets/imgs/btc.png">\n        </ion-col>\n        <ion-col col-7>\n          <ion-row>\n            <h6>McDonalds Shanti Niketan</h6>\n          </ion-row>\n          <ion-row>\n            <div style="font-size:10px;">09 Dec 2017</div>\n          </ion-row>\n        </ion-col>\n        <ion-col col-3>\n          <ion-row>\n            <h6>INR 540</h6>\n          </ion-row>\n          <ion-row>\n            <div style="font-size:8.7px;">BTC 0.0005131</div>\n          </ion-row>\n        </ion-col>\n\n      </ion-row>\n    </ion-card>\n\n    <ion-card>\n      <ion-row>\n        <ion-col col-2>\n          <img src="../../assets/imgs/eth.png">\n        </ion-col>\n        <ion-col col-7>\n          <ion-row>\n            <h6>Starbucks-Sec 63</h6>\n          </ion-row>\n          <ion-row>\n            <div style="font-size:10px;">10 Dec 2017</div>\n          </ion-row>\n        </ion-col>\n        <ion-col col-3>\n          <ion-row>\n            <h6>INR 730</h6>\n          </ion-row>\n          <ion-row>\n            <div style="font-size:8.7px;">ETC 0.0147</div>\n          </ion-row>\n        </ion-col>\n\n      </ion-row>\n    </ion-card>\n\n    <ion-card>\n      <ion-row>\n        <ion-col col-2>\n          <img src="../../assets/imgs/omise_logo.png">\n        </ion-col>\n        <ion-col col-7>\n          <ion-row>\n            <h6>Fashion Point- TGIP, Sec-18</h6>\n          </ion-row>\n          <ion-row>\n            <div style="font-size:10px;">13 Dec 2007</div>\n          </ion-row>\n        </ion-col>\n        <ion-col col-3>\n          <ion-row>\n            <h6>INR 3460</h6>\n          </ion-row>\n          <ion-row>\n            <div style="font-size:8.7px;">OMG 3.8030</div>\n          </ion-row>\n        </ion-col>\n\n      </ion-row>\n    </ion-card>\n\n    <ion-card>\n      <ion-row>\n        <ion-col col-2>\n          <img src="../../assets/imgs/ltc.png">\n        </ion-col>\n        <ion-col col-7>\n          <ion-row>\n            <h6>Bata - Bata Chowk</h6>\n          </ion-row>\n          <ion-row>\n            <div style="font-size:10px;">11 Dec 2007</div>\n          </ion-row>\n        </ion-col>\n        <ion-col col-3>\n          <ion-row>\n            <h6>INR 400</h6>\n          </ion-row>\n          <ion-row>\n            <div style="font-size:8.7px;">LTC 0.0223</div>\n          </ion-row>\n        </ion-col>\n\n      </ion-row>\n    </ion-card>\n    <ion-card>\n        <ion-row>\n          <ion-col col-2>\n            <img src="../../assets/imgs/btc.png">\n          </ion-col>\n          <ion-col col-7>\n            <ion-row>\n              <h6>McDonalds Shanti Niketan</h6>\n            </ion-row>\n            <ion-row>\n              <div style="font-size:10px;">09 Dec 2017</div>\n            </ion-row>\n          </ion-col>\n          <ion-col col-3>\n            <ion-row>\n              <h6>INR 540</h6>\n            </ion-row>\n            <ion-row>\n              <div style="font-size:8.7px;">BTC 0.0005131</div>\n            </ion-row>\n          </ion-col>\n  \n        </ion-row>\n      </ion-card>\n  \n      <ion-card>\n        <ion-row>\n          <ion-col col-2>\n            <img src="../../assets/imgs/eth.png">\n          </ion-col>\n          <ion-col col-7>\n            <ion-row>\n              <h6>Apple Store Niketan- Shanti</h6>\n            </ion-row>\n            <ion-row>\n              <div style="font-size:10px;">12 Dec 2017</div>\n            </ion-row>\n          </ion-col>\n          <ion-col col-3>\n            <ion-row>\n              <h6>INR 72000</h6>\n            </ion-row>\n            <ion-row>\n              <div style="font-size:8.7px;">BTC 0.06842</div>\n            </ion-row>\n          </ion-col>\n  \n        </ion-row>\n      </ion-card>\n      <ion-card>\n          <ion-row>\n            <ion-col col-2>\n              <img src="../../assets/imgs/btc.png">\n            </ion-col>\n            <ion-col col-7>\n              <ion-row>\n                <h6>McDonalds Shanti Niketan</h6>\n              </ion-row>\n              <ion-row>\n                <div style="font-size:10px;">09 Dec 2017</div>\n              </ion-row>\n            </ion-col>\n            <ion-col col-3>\n              <ion-row>\n                <h6>INR 540</h6>\n              </ion-row>\n              <ion-row>\n                <div style="font-size:8.7px;">BTC 0.0005131</div>\n              </ion-row>\n            </ion-col>\n    \n          </ion-row>\n        </ion-card>\n\n    </div>\n\n</ion-content>\n<ion-footer>\n    <ion-row style="border-top:1px solid #252933; margin:0 0 2px 0">\n        <ion-col col-3 style="text-align:center">\n            <a>\n                <img class="btns-img" src="../../assets/imgs/portfolio_icon.png">\n                <span>Portfolio</span>\n            </a>\n\n        </ion-col>\n        <ion-col col-3 style="text-align:center">\n            <a>\n                <img class="btns-img-swap" src="../../assets/imgs/converter_icon.png">\n                <span>Swap</span>\n            </a>\n        </ion-col>\n        <ion-col col-3 style="text-align:center">\n            <a>\n                <img class="btns-img" src="../../assets/imgs/send_receive_icon.png">\n                <span>Send/ Receive</span>\n            </a>\n        </ion-col>\n        <ion-col col-3 style="text-align:center">\n            <a>\n                <img class="btns-img" src="../../assets/imgs/credit_card.png">\n                <span>Card</span>\n            </a>\n        </ion-col>\n\n    </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/transactions/transactions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], TransactionsPage);
    return TransactionsPage;
}());

//# sourceMappingURL=transactions.js.map

/***/ })

});
//# sourceMappingURL=2.js.map