webpackJsonp([33],{

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BtcBackupPageModule", function() { return BtcBackupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__btc_backup__ = __webpack_require__(318);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BtcBackupPageModule = /** @class */ (function () {
    function BtcBackupPageModule() {
    }
    BtcBackupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__btc_backup__["a" /* BtcBackupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__btc_backup__["a" /* BtcBackupPage */]),
            ],
        })
    ], BtcBackupPageModule);
    return BtcBackupPageModule;
}());

//# sourceMappingURL=btc-backup.module.js.map

/***/ }),

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtcBackupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BtcBackupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BtcBackupPage = /** @class */ (function () {
    function BtcBackupPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BtcBackupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BtcBackupPage');
    };
    BtcBackupPage.prototype.sharing = function () {
        this.navCtrl.setRoot('BtcYourprivatekeyPage');
    };
    BtcBackupPage.prototype.back = function () {
        this.navCtrl.setRoot('BtcPassphrasePage');
    };
    BtcBackupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-btc-backup',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-backup/btc-backup.html"*/'<ion-content class="Background">\n    <ion-row>\n      <ion-col col-6 text-left>\n        <div class="close-btn" (click)=back()>X</div>\n      </ion-col>\n      <ion-col col-6 text-right>\n        <img class="img" width="40px" height="40px" src="./assets/imgs/bitcoincash_logo.png">\n      </ion-col>\n    </ion-row>\n    \n  \n      <ion-row>\n        <ion-col class="heading">\n          Backup Your Wallet\n        </ion-col>\n      </ion-row>\n  \n      <ion-row>\n        <ion-col class="icon">\n          <ion-icon name="key"></ion-icon>\n        </ion-col>\n      </ion-row>\n  \n      <ion-row>\n        <ion-col class="info">\n          Backup lets you store your UTC file and 4 Digit Pin\n          <br> in your email inbox folder. It can be used to restore\n          <br> your Ether and Tokens if your device is lost or damaged.\n        </ion-col>\n      </ion-row>\n  \n      <ion-row>\n        <ion-col class="icon">\n          <ion-icon name="paper"></ion-icon>\n        </ion-col>\n      </ion-row>\n  \n      <ion-row>\n        <ion-col class="info">\n          To Backup your wallet you just have to enter\n          <br> your 4 Digit Pin. An email picker will apper with\n          <br> attached UTC file.\n        </ion-col>\n      </ion-row>\n  \n      <ion-row>\n        <ion-col class="icon">\n          <ion-icon name="warning"></ion-icon>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class="warning">\n          "warning"\n        </ion-col>\n      </ion-row>\n  \n      <ion-row>\n        <ion-col class="warninfo">\n          Anyone with a copy of the "Backup UTC File"\n          <br> and your Passphrase can restore your wallet.\n        </ion-col>\n      </ion-row>\n  \n      <ion-row>\n        <ion-col col-1>\n        </ion-col>\n        <ion-col col-10>\n          <!-- <a [href]="fileName" download="{{file}}" block>Download File</a> -->\n          <!-- <button ion-button [href]="fileName" download="{{file}}" block>Download File</button> -->\n        </ion-col>\n  \n        <ion-col col-1>\n        </ion-col>\n      </ion-row>\n  \n      <ion-row>\n        <ion-col col-1>\n        </ion-col>\n        <ion-col col-10>\n          <button block ion-button (click)="sharing()">Backup Now</button>\n        </ion-col>\n        <ion-col col-1>\n        </ion-col>\n      </ion-row>\n  \n  </ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/btc-backup/btc-backup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], BtcBackupPage);
    return BtcBackupPage;
}());

//# sourceMappingURL=btc-backup.js.map

/***/ })

});
//# sourceMappingURL=33.js.map