webpackJsonp([21],{

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnterPrivateKeyPageModule", function() { return EnterPrivateKeyPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__enter_private_key__ = __webpack_require__(330);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EnterPrivateKeyPageModule = /** @class */ (function () {
    function EnterPrivateKeyPageModule() {
    }
    EnterPrivateKeyPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__enter_private_key__["a" /* EnterPrivateKeyPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__enter_private_key__["a" /* EnterPrivateKeyPage */]),
            ],
        })
    ], EnterPrivateKeyPageModule);
    return EnterPrivateKeyPageModule;
}());

//# sourceMappingURL=enter-private-key.module.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EnterPrivateKeyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the EnterPrivateKeyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EnterPrivateKeyPage = /** @class */ (function () {
    function EnterPrivateKeyPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EnterPrivateKeyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EnterPrivateKeyPage');
    };
    EnterPrivateKeyPage.prototype.back = function () {
        this.navCtrl.setRoot('PrivatekeyRestorePage');
    };
    EnterPrivateKeyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-enter-private-key',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/enter-private-key/enter-private-key.html"*/'<ion-content class="Background">\n  <div class="close-btn" (click)=back()>X</div>\n\n  <ion-row>\n    <ion-col class="info">\n      A private key is a secret key of your \n      <br>wallet that is used to generate a public\n      <br>key. This public key is your address\n      <br>on the Ethereum blockchain. The private   \n      <br>key also allows you to sign transactions.\n    </ion-col>\n  </ion-row>\n  <ion-row style="margin-top: 50px">\n    <ion-col col-1></ion-col>\n      <ion-col col-9>\n        <ion-input  class="Passphrase" type="text" [(ngModel)]="privatekey" name="privatekey" placeholder="Enter Your Private Key"></ion-input>\n      </ion-col>\n      <ion-col col-1 class="paste">\n          <ion-icon name="clipboard"></ion-icon>\n      </ion-col>\n      <ion-col col-1></ion-col>\n    </ion-row>\n\n    <ion-row>\n        <ion-col style="margin: 16px 25px!important;">\n          <button block ion-button (click)="restore()">RESTORE</button>\n        </ion-col>\n      </ion-row>\n\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/enter-private-key/enter-private-key.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], EnterPrivateKeyPage);
    return EnterPrivateKeyPage;
}());

//# sourceMappingURL=enter-private-key.js.map

/***/ })

});
//# sourceMappingURL=21.js.map