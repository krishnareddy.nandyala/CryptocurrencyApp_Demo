webpackJsonp([13],{

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EthWalletPageModule", function() { return EthWalletPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__eth_wallet__ = __webpack_require__(318);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EthWalletPageModule = /** @class */ (function () {
    function EthWalletPageModule() {
    }
    EthWalletPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__eth_wallet__["a" /* EthWalletPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__eth_wallet__["a" /* EthWalletPage */]),
            ],
        })
    ], EthWalletPageModule);
    return EthWalletPageModule;
}());

//# sourceMappingURL=eth-wallet.module.js.map

/***/ }),

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EthWalletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EthWalletPage = /** @class */ (function () {
    function EthWalletPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EthWalletPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EthWalletPage');
    };
    EthWalletPage.prototype.addWallet = function () {
        this.navCtrl.setRoot('EthWalletCreatePage');
    };
    EthWalletPage.prototype.back = function () {
        this.navCtrl.setRoot('YourTokensPage');
    };
    EthWalletPage.prototype.walletviewPage = function () {
        this.navCtrl.setRoot('EthWalletviewPage');
    };
    EthWalletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-eth-wallet',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/eth-wallet/eth-wallet.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-row>\n      <ion-col col-1>\n        <div class="close-btn" (click)=back()>X</div>\n      </ion-col>\n      <ion-col col-10>\n        <ion-title text-center>Wallet</ion-title>\n      </ion-col>\n      <ion-col col-1>\n      </ion-col>\n    </ion-row>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="Background">\n  <ion-row style="border-bottom: 1px solid lightslategray;" (click)="walletviewPage()">\n    <ion-col col-2>\n      <ion-icon name="contact" width="50px" height="50px"></ion-icon>\n    </ion-col>\n    <ion-col col-8>\n      <h6>Name</h6>\n      <ion-scroll scrollX="true" style="width: 100%; height:25%; color: #fff;">\n        121544&*97&%^%$%5454115&^%7568\n      </ion-scroll>\n    </ion-col>\n    <ion-col col-2>\n      <ion-icon name="contact" width="50px" height="50px"></ion-icon>\n    </ion-col>\n  </ion-row>\n  <ion-row style="border-bottom: 1px solid lightslategray;">\n    <ion-col col-2>\n      <ion-icon name="contact" width="50px" height="50px"></ion-icon>\n    </ion-col>\n    <ion-col col-8>\n      <h6>Name</h6>\n      <ion-scroll scrollX="true" style="width: 100%; height:25%; color: #fff;">\n        fdasfdsafnnnnnnnnnnnnnnnnnnnnnnnnnnnnnfda fdsaf dsafdsa fsdaf dfdsnnnnnndas fdaf asdf dasfdsa fdsa\n      </ion-scroll>\n    </ion-col>\n    <ion-col col-2>\n      <ion-icon name="contact" width="50px" height="50px"></ion-icon>\n    </ion-col>\n  </ion-row>\n</ion-content>\n<ion-footer>\n  <ion-row>\n\n    <ion-col col-10>\n\n    </ion-col>\n    <ion-col col-2>\n      <ion-icon name="add-circle" (click)="addWallet()"></ion-icon>\n    </ion-col>\n  </ion-row>\n</ion-footer>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/eth-wallet/eth-wallet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], EthWalletPage);
    return EthWalletPage;
}());

//# sourceMappingURL=eth-wallet.js.map

/***/ })

});
//# sourceMappingURL=13.js.map