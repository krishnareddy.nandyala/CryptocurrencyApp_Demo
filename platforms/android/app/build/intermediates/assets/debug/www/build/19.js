webpackJsonp([19],{

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BackupWalletPageModule", function() { return BackupWalletPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__backup_wallet__ = __webpack_require__(312);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BackupWalletPageModule = /** @class */ (function () {
    function BackupWalletPageModule() {
    }
    BackupWalletPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__backup_wallet__["a" /* BackupWalletPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__backup_wallet__["a" /* BackupWalletPage */]),
            ],
        })
    ], BackupWalletPageModule);
    return BackupWalletPageModule;
}());

//# sourceMappingURL=backup-wallet.module.js.map

/***/ }),

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BackupWalletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_common_common__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(195);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import {File} from '@ionic-native/file/ngx';
var BackupWalletPage = /** @class */ (function () {
    function BackupWalletPage(navCtrl, navParams, commonProvider, socialSharing, platform, file) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.commonProvider = commonProvider;
        this.socialSharing = socialSharing;
        this.platform = platform;
        this.file = file;
        setTimeout(function () {
            _this.key = _this.commonProvider.privateKey;
            _this.file = _this.commonProvider.keyFile;
            _this.fileName = _this.commonProvider.fileName;
        }, 2000);
    }
    BackupWalletPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BackupWalletPage');
    };
    BackupWalletPage.prototype.back = function () {
        this.navCtrl.setRoot('HomePage');
    };
    // downloadFile() {
    //   this. path = this.file.dataDirectory;
    //   // if (this.platform.is(this.ios)) {
    //   //   path = this.file.documentsDirectory;
    //   // }
    //   // else {
    //   //   path = this.file.dataDirectory;
    //   // }
    //   const transfer = this.transfer.create();
    //   const url = encodeURI(`${this.file}`);
    //    this.fileName = 'img' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.jpeg'
    //   this.storageDirectory = this.file;
    //   transfer.download(url, this.storageDirectory + this.file).then((entry) => {
    //     alert('Image Downloaded')
    //     console.log('success');
    //   }, (error) => {
    //     alert('Error')
    //     console.log('error');
    //   });
    // }
    BackupWalletPage.prototype.sharing = function () {
        //const url = encodeURI(`${this.commonProvider.unEncodedFile}`);
        // console.log(this.commonProvider.unEncodedFile);
        // this.socialSharing.share(null, 'Secured Wallet', null,this.commonProvider.unEncodedFile)
        //   .then((data) => {
        //     this.navCtrl.setRoot('YourTokensPage');
        //   }).catch((err) => {
        //     console.log(err);
        //   })
        this.navCtrl.setRoot('YourTokensPage');
    };
    BackupWalletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-backup-wallet',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/eth-backup-wallet/backup-wallet.html"*/'<ion-content class="Background">\n  <div class="close-btn" (click)=back()>X</div>\n\n  <ion-row>\n    <ion-col class="heading">\n      Backup Your Wallet\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col class="icon">\n      <ion-icon name="key"></ion-icon>\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col class="info">\n      Backup lets you store your UTC file and 4 Digit Pin\n      <br> in your email inbox folder. It can be used to restore\n      <br> your Ether and Tokens if your device is lost or damaged.\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col class="icon">\n      <ion-icon name="paper"></ion-icon>\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col class="info">\n      To Backup your wallet you just have to enter\n      <br> your 4 Digit Pin. An email picker will apper with\n      <br> attached UTC file.\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col class="icon">\n      <ion-icon name="warning"></ion-icon>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col class="warning">\n      "warning"\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col class="warninfo">\n      Anyone with a copy of the "Backup UTC File"\n      <br> and your Passphrase can restore your wallet.\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n      <ion-col col-1>\n      </ion-col>\n      <ion-col col-10>\n        <!-- <a [href]="fileName" download="{{file}}" block>Download File</a> -->\n        <!-- <button ion-button [href]="fileName" download="{{file}}" block>Download File</button> -->\n      </ion-col>\n      \n      <ion-col col-1>\n      </ion-col>\n    </ion-row>\n\n  <ion-row>\n    <ion-col col-1>\n    </ion-col>\n    <ion-col col-10>\n      <button block ion-button (click)="sharing()">Backup Now</button>\n    </ion-col>\n    <ion-col col-1>\n    </ion-col>\n  </ion-row>\n\n\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/eth-backup-wallet/backup-wallet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_common_common__["a" /* CommonProvider */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */]])
    ], BackupWalletPage);
    return BackupWalletPage;
}());

//# sourceMappingURL=backup-wallet.js.map

/***/ })

});
//# sourceMappingURL=19.js.map