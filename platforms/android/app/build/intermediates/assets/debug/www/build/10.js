webpackJsonp([10],{

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortfolioPageModule", function() { return PortfolioPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__portfolio__ = __webpack_require__(342);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PortfolioPageModule = /** @class */ (function () {
    function PortfolioPageModule() {
    }
    PortfolioPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__portfolio__["a" /* PortfolioPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__portfolio__["a" /* PortfolioPage */]),
            ],
        })
    ], PortfolioPageModule);
    return PortfolioPageModule;
}());

//# sourceMappingURL=portfolio.module.js.map

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PortfolioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PortfolioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PortfolioPage = /** @class */ (function () {
    function PortfolioPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PortfolioPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PortfolioPage');
    };
    PortfolioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-portfolio',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/portfolio/portfolio.html"*/'\n\n\n<ion-content class="Background">\n    <ion-row>\n        <ion-col col-6 left>\n          <button ion-button menuToggle>\n            <img src="../../assets/imgs/menu.png">\n          </button>\n        </ion-col>\n        <ion-col col-6 style="text-align: right;">\n          <button ion-button menuToggle>\n            <img src="../../assets/imgs/sign-code.png">\n          </button>\n        </ion-col>\n      </ion-row>\n\n      <span>\n        <h6>Currency</h6>\n        <h3>EXCHANGE</h3>\n        </span>\n\n        <div class="span-back">\n\n\n        </div>\n      \n</ion-content>\n\n<ion-footer>\n    <ion-row style="border-top:1px solid #252933; margin:0 0 2px 0">\n        <ion-col col-3 style="text-align:center">\n            <a>\n                <img class="btns-img" src="../../assets/imgs/portfolio_icon.png">\n                <span>Portfolio</span>\n            </a>\n\n        </ion-col>\n        <ion-col col-3 style="text-align:center">\n            <a>\n                <img class="btns-img-swap" src="../../assets/imgs/converter_icon.png">\n                <span>Swap</span>\n            </a>\n        </ion-col>\n        <ion-col col-3 style="text-align:center">\n            <a>\n                <img class="btns-img" src="../../assets/imgs/send_receive_icon.png">\n                <span>Send/ Receive</span>\n            </a>\n        </ion-col>\n        <ion-col col-3 style="text-align:center">\n            <a>\n                <img class="btns-img" src="../../assets/imgs/credit_card.png">\n                <span>Card</span>\n            </a>\n        </ion-col>\n\n    </ion-row>\n\n</ion-footer>\n'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/portfolio/portfolio.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], PortfolioPage);
    return PortfolioPage;
}());

//# sourceMappingURL=portfolio.js.map

/***/ })

});
//# sourceMappingURL=10.js.map