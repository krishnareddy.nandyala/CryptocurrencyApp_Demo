webpackJsonp([22],{

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function() { return DashboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard__ = __webpack_require__(309);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DashboardPageModule = /** @class */ (function () {
    function DashboardPageModule() {
    }
    DashboardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */]),
            ],
        })
    ], DashboardPageModule);
    return DashboardPageModule;
}());

//# sourceMappingURL=dashboard.module.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DashboardPage = /** @class */ (function () {
    function DashboardPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DashboardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DashboardPage');
    };
    DashboardPage.prototype.slideChanged = function () {
        var currentIndex = this.slides.getActiveIndex();
        console.log('Current index is', currentIndex);
    };
    DashboardPage.prototype.routingETH = function () {
        this.navCtrl.setRoot("HomePage");
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Slides */])
    ], DashboardPage.prototype, "slides", void 0);
    DashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dashboard',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/dashboard/dashboard.html"*/'<!-- <ion-header class="Background">\n    <ion-row>\n        <ion-col col-6 left>\n            <button ion-button menuToggle>\n                <img src="../../assets/imgs/menu.png">\n            </button>\n        </ion-col>\n        <ion-col col-6 style="text-align: right;">\n            <button ion-button menuToggle>\n                <img src="../../assets/imgs/sign-code.png">\n            </button>\n        </ion-col>\n    </ion-row> -->\n<!-- <div>\n         <ion-slides pager>\n            <ion-slide>\n                <img src="../../assets/imgs/slide1.png">\n            </ion-slide>\n            <ion-slide>\n                <img src="../../assets/imgs/slide2.png">\n            </ion-slide>\n            <ion-slide>\n                <img src="../../assets/imgs/slide3.png">\n            </ion-slide>\n            <ion-slide>\n                <img src="../../assets/imgs/slide4.png">\n                <ion-col col-4>\n                    <ion-col> -->\n<!-- <div>\n                            <ion-row>\n                                <ion-col col-4>\n                                </ion-col>\n                                <ion-col col-4>\n                                    <div style="border:1px solid #fff; background: #333c5a; border-radius: 20px; padding:10px; color: #fff; text-align: center; margin-top: 10px;">LINKED</div>\n                                </ion-col>\n                                <ion-col col-4>\n                                </ion-col>\n                            </ion-row>\n                        </div> -->\n\n<!-- \n                        <ion-row style="border-top:1px solid #252933; margin:9px 0 5px 0">\n                            <ion-col col-3 style="text-align:center">\n                                <a>\n                                    <img class="btns-img" src="../../assets/imgs/portfolio_icon.png">\n                                    <span>Portfolio</span>\n                                </a>\n\n                            </ion-col>\n                            <ion-col col-3 style="text-align:center">\n                                <a>\n                                    <img class="btns-img-swap" src="../../assets/imgs/converter_icon.png">\n                                    <span>Swap</span>\n                                </a>\n                            </ion-col>\n                            <ion-col col-3 style="text-align:center">\n                                <a>\n                                    <img class="btns-img" src="../../assets/imgs/send_receive_icon.png">\n                                    <span>Send/ Receive</span>\n                                </a>\n                            </ion-col>\n                            <ion-col col-3 style="text-align:center">\n                                <a>\n                                    <img class="btns-img" src="../../assets/imgs/credit_card.png">\n                                    <span>Card</span>\n                                </a>\n                            </ion-col>\n\n                        </ion-row> -->\n<!-- </ion-col>\n                </ion-col>\n            </ion-slide>\n        </ion-slides> \n     </div> -->\n<!-- </ion-header> -->\n<!-- <ion-content class="Background">\n    <div> -->\n<!-- <ion-card>\n            <ion-row>\n                <ion-col col-2>\n                    <img src="../../assets/imgs/btc.png">\n                </ion-col>\n                <ion-col col-7>\n                    <ion-row>\n                        <h6>McDonalds Shanti Niketan</h6>\n                    </ion-row>\n                    <ion-row>\n                        <div style="font-size:10px;">09 Dec 2017</div>\n                    </ion-row>\n                </ion-col>\n                <ion-col col-3>\n                    <ion-row>\n                        <h6>INR 540</h6>\n                    </ion-row>\n                    <ion-row>\n                        <div style="font-size:8.7px;">BTC 0.0005131</div>\n                    </ion-row>\n                </ion-col>\n\n            </ion-row>\n        </ion-card> -->\n<!-- <ion-card>\n            <ion-row>\n                <ion-col col-2>\n                    <img src="../../assets/imgs/btc.png">\n                </ion-col>\n                <ion-col col-8>\n                    <ion-row>\n                        <h4>LTC</h4>\n                    </ion-row>\n                   \n                </ion-col>\n               \n\n            </ion-row>\n        </ion-card> \n\n\n        <ion-card>\n            <ion-row>\n                <ion-col col-2>\n                    <img src="../../assets/imgs/eth.png">\n                </ion-col>\n                <ion-col col-7>\n                    <ion-row>\n                        <h6>Starbucks-Sec63</h6>\n                    </ion-row>\n                    <ion-row>\n                        <div style="font-size:10px;">10 Dec 2017</div>\n                    </ion-row>\n                </ion-col>\n                <ion-col col-3>\n                    <ion-row>\n                        <h6>INR 730</h6>\n                    </ion-row>\n                    <ion-row>\n                        <div style="font-size:8.7px;">ETC 0.0147</div>\n                    </ion-row>\n                </ion-col>\n\n            </ion-row>\n        </ion-card>\n\n        <ion-card>\n            <ion-row>\n                <ion-col col-2>\n                    <img src="../../assets/imgs/ltc.png">\n                </ion-col>\n                <ion-col col-7>\n                    <ion-row>\n                        <h6>Bata - Bata Chowk</h6>\n                    </ion-row>\n                    <ion-row>\n                        <div style="font-size:10px;">10 Dec 2017</div>\n                    </ion-row>\n                </ion-col>\n                <ion-col col-3>\n                    <ion-row>\n                        <h6>INR 400</h6>\n                    </ion-row>\n                    <ion-row>\n                        <div style="font-size:8.7px;">LTC 0.0223</div>\n                    </ion-row>\n                </ion-col>\n\n            </ion-row>\n        </ion-card>\n        <ion-card>\n            <ion-row>\n                <ion-col col-2>\n                    <img src="../../assets/imgs/btc.png">\n                </ion-col>\n                <ion-col col-7>\n                    <ion-row>\n                        <h6>Apple Store Niketan- Shanti</h6>\n                    </ion-row>\n                    <ion-row>\n                        <div style="font-size:10px;">12 Dec 2017</div>\n                    </ion-row>\n                </ion-col>\n                <ion-col col-3>\n                    <ion-row>\n                        <h6>INR 72000</h6>\n                    </ion-row>\n                    <ion-row>\n                        <div style="font-size:8.7px;">BTC 0.06842</div>\n                    </ion-row>\n                </ion-col>\n\n            </ion-row>\n        </ion-card>\n        <ion-card>\n            <ion-row>\n                <ion-col col-2>\n                    <img src="../../assets/imgs/omise_logo.png">\n                </ion-col>\n                <ion-col col-7>\n                    <ion-row>\n                        <h6>Fashion Point - TGIP,Sec-18</h6>\n                    </ion-row>\n                    <ion-row>\n                        <div style="font-size:10px;">13 Dec 2017</div>\n                    </ion-row>\n                </ion-col>\n                <ion-col col-3>\n                    <ion-row>\n                        <h6>INR 3460</h6>\n                    </ion-row>\n                    <ion-row>\n                        <div style="font-size:8.7px;">OMG 3.6030</div>\n                    </ion-row>\n                </ion-col>\n\n            </ion-row>\n        </ion-card>\n\n\n    </div>\n\n</ion-content> -->\n\n\n\n<ion-content class="Background">\n    <ion-row style="margin-top: 70px">\n        <ion-col col-1></ion-col>\n        <ion-col col-10>\n            <img (click)="routingETH()" height="auto" width="500px" src="./assets/imgs/eth-img.png">\n        </ion-col>\n        <ion-col col-1></ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col col-1></ion-col>\n        <ion-col col-10>\n            <img height="auto" width="500px" src="./assets/imgs/btc-img.png">\n        </ion-col>\n        <ion-col col-1></ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col col-1></ion-col>\n        <ion-col col-10>\n            <img height="auto" width="500px" src="./assets/imgs/ltc-img.png">\n        </ion-col>\n        <ion-col col-1></ion-col>\n    </ion-row>\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/dashboard/dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], DashboardPage);
    return DashboardPage;
}());

//# sourceMappingURL=dashboard.js.map

/***/ })

});
//# sourceMappingURL=22.js.map