webpackJsonp([26],{

/***/ 110:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 110;

/***/ }),

/***/ 150:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-eth-wallet/add-eth-wallet.module": [
		271,
		25
	],
	"../pages/card/card.module": [
		272,
		24
	],
	"../pages/confirm-password/confirm-password.module": [
		273,
		23
	],
	"../pages/dashboard/dashboard.module": [
		274,
		22
	],
	"../pages/enter-private-key/enter-private-key.module": [
		275,
		21
	],
	"../pages/eth-addwalletfromprivatekey/eth-addwalletfromprivatekey.module": [
		276,
		20
	],
	"../pages/eth-backup-wallet/backup-wallet.module": [
		277,
		19
	],
	"../pages/eth-home/home.module": [
		278,
		18
	],
	"../pages/eth-importwalletfromjsonfile/eth-importwalletfromjsonfile.module": [
		279,
		17
	],
	"../pages/eth-passphrase/passphrase.module": [
		280,
		16
	],
	"../pages/eth-termsof-service/termsof-service.module": [
		281,
		15
	],
	"../pages/eth-wallet-create/eth-wallet-create.module": [
		282,
		14
	],
	"../pages/eth-wallet/eth-wallet.module": [
		283,
		13
	],
	"../pages/eth-walletview/eth-walletview.module": [
		284,
		12
	],
	"../pages/eth-your-tokens/your-tokens.module": [
		285,
		0
	],
	"../pages/password/password.module": [
		286,
		11
	],
	"../pages/portfolio/portfolio.module": [
		287,
		10
	],
	"../pages/privatekey-restore/privatekey-restore.module": [
		288,
		9
	],
	"../pages/restore/restore.module": [
		289,
		8
	],
	"../pages/send-receive/send-receive.module": [
		290,
		7
	],
	"../pages/show-private-key/show-private-key.module": [
		291,
		6
	],
	"../pages/startpage/startpage.module": [
		292,
		5
	],
	"../pages/swap/swap.module": [
		293,
		4
	],
	"../pages/tabs/tabs.module": [
		294,
		3
	],
	"../pages/transactions/transactions.module": [
		295,
		2
	],
	"../pages/utc-file-restore/utc-file-restore.module": [
		296,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 150;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import {QRCodeComponent} from 'angular2-qrcode'
var params = { keyBytes: 32, ivBytes: 16 };
var kdf = "pbkdf2";
// @Injectable({
//   providedIn: 'root'
// })
var CommonProvider = /** @class */ (function () {
    // constructor(public http: HttpClient) {
    //   console.log('Hello CommonProvider Provider');
    function CommonProvider(sanitizer) {
        this.sanitizer = sanitizer;
        this.password = '';
        this.privateKey = '';
        this.address = '';
        this.fileName = '';
        this.unEncodedFile = '';
    }
    // isGenerated: boolean = false;
    // isGenerating: boolean = false;
    //title = 'app';
    // elementType = 'url';
    // value = 'Techiediaries';
    CommonProvider.prototype.generateKey = function (Passphrase) {
        var _this = this;
        // this.isGenerating = true;
        //var password = this.password;
        var kdf = "pbkdf2";
        var dk = keythereum.create();
        var options = {
            kdf: "pbkdf2",
            cipher: "aes-128-ctr",
            kdfparams: {
                c: 262144,
                dklen: 32,
                prf: "hmac-sha256"
            }
        };
        // synchronous
        keythereum.dump(Passphrase, dk.privateKey, dk.salt, dk.iv, options, function (cb, err) {
            _this.keyObject = cb;
            keythereum.exportToFile(_this.keyObject);
            console.log(_this.keyObject);
            _this.address = '0x' + _this.keyObject.address;
            _this.unEncodedFile = "data:text;charset=utf-8," + encodeURIComponent(JSON.stringify(_this.keyObject));
            _this.keyFile = _this.sanitizer.bypassSecurityTrustUrl("data:text;charset=utf-8," + encodeURIComponent(JSON.stringify(_this.keyObject)));
            _this.fileName = "UTC--" + (new Date).toISOString() + "--" + _this.keyObject.address + ".json";
            // this.isGenerated = true;
            // this.isGenerating = false;
            var privateKey = keythereum.recover(Passphrase, _this.keyObject);
            console.log(privateKey.toString('hex'));
            _this.privateKey = privateKey.toString('hex');
        });
    };
    ;
    CommonProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
    ], CommonProvider);
    return CommonProvider;
}());

//# sourceMappingURL=common.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(219);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_common_common__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__ = __webpack_require__(195);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





//import { NgxQRCodeModule } from 'ngx-qrcode2';
// import { ListPage } from '../pages/list/list';




// import { IonDigitKeyboard } from '../components/ion-digit-keyboard/ion-digit-keyboard.module';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/add-eth-wallet/add-eth-wallet.module#AddEthWalletPageModule', name: 'AddEthWalletPage', segment: 'add-eth-wallet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/card/card.module#CardPageModule', name: 'CardPage', segment: 'card', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/confirm-password/confirm-password.module#ConfirmPasswordPageModule', name: 'ConfirmPasswordPage', segment: 'confirm-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/enter-private-key/enter-private-key.module#EnterPrivateKeyPageModule', name: 'EnterPrivateKeyPage', segment: 'enter-private-key', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eth-addwalletfromprivatekey/eth-addwalletfromprivatekey.module#EthAddwalletfromprivatekeyPageModule', name: 'EthAddwalletfromprivatekeyPage', segment: 'eth-addwalletfromprivatekey', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eth-backup-wallet/backup-wallet.module#BackupWalletPageModule', name: 'BackupWalletPage', segment: 'backup-wallet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eth-home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eth-importwalletfromjsonfile/eth-importwalletfromjsonfile.module#EthImportwalletfromjsonfilePageModule', name: 'EthImportwalletfromjsonfilePage', segment: 'eth-importwalletfromjsonfile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eth-passphrase/passphrase.module#PassphrasePageModule', name: 'PassphrasePage', segment: 'passphrase', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eth-termsof-service/termsof-service.module#TermsofServicePageModule', name: 'TermsofServicePage', segment: 'termsof-service', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eth-wallet-create/eth-wallet-create.module#EthWalletCreatePageModule', name: 'EthWalletCreatePage', segment: 'eth-wallet-create', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eth-wallet/eth-wallet.module#EthWalletPageModule', name: 'EthWalletPage', segment: 'eth-wallet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eth-walletview/eth-walletview.module#EthWalletviewPageModule', name: 'EthWalletviewPage', segment: 'eth-walletview', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eth-your-tokens/your-tokens.module#YourTokensPageModule', name: 'YourTokensPage', segment: 'your-tokens', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/password/password.module#PasswordPageModule', name: 'PasswordPage', segment: 'password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/portfolio/portfolio.module#PortfolioPageModule', name: 'PortfolioPage', segment: 'portfolio', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/privatekey-restore/privatekey-restore.module#PrivatekeyRestorePageModule', name: 'PrivatekeyRestorePage', segment: 'privatekey-restore', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/restore/restore.module#RestorePageModule', name: 'RestorePage', segment: 'restore', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/send-receive/send-receive.module#SendReceivePageModule', name: 'SendReceivePage', segment: 'send-receive', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/show-private-key/show-private-key.module#ShowPrivateKeyPageModule', name: 'ShowPrivateKeyPage', segment: 'show-private-key', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/startpage/startpage.module#StartpagePageModule', name: 'StartpagePage', segment: 'startpage', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/swap/swap.module#SwapPageModule', name: 'SwapPage', segment: 'swap', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/transactions/transactions.module#TransactionsPageModule', name: 'TransactionsPage', segment: 'transactions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/utc-file-restore/utc-file-restore.module#UtcFileRestorePageModule', name: 'UtcFileRestorePage', segment: 'utc-file-restore', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_5__providers_common_common__["a" /* CommonProvider */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(192);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = 'PasswordPage';
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            // { title: 'List', component: ListPage }
            { title: 'Dashboard', component: "DashboardPage", icon: '../assets/icon/home.png', },
            { title: 'Manage Account', component: "DashboardPage", icon: '../assets/icon/manage.png' },
            { title: 'Porfolio', component: "PortfolioPage", icon: '../assets/icon/bag.png' },
            { title: 'Transactions', component: "TransactionsPage", icon: '../assets/icon/tranactions.png' },
            { title: 'Wallet', component: 'DashboardPage', icon: '../assets/icon/wallet.png' },
            { title: 'Security', component: 'DashboardPage', icon: '../assets/icon/security.png' },
            //{ title: 'Changelog', component: 'DashboardPage',icon:''},
            { title: 'Support', component: 'DashboardPage', icon: '../assets/icon/support.png' },
            { title: 'Logout', component: 'DashboardPage', icon: '../assets/icon/logout.png' },
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/app/app.html"*/'<ion-menu [content]="content">\n  <!-- <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header> -->\n\n  <ion-content class="bck-img">\n    <ion-list>\n      <ion-row>\n        <ion-col col-3>\n\n        </ion-col>\n        <ion-col col-6 style="text-align:center;">\n            <img style="margin-top: 20px;border-radius:80px; height:80px; width:80px; text-align: center;" \n            src="../../assets/imgs/no-profile-img.gif">\n            <p>USER NAME</p>\n            <p style="color:#05b4e9">VAIBHAV</p>\n        </ion-col>\n        <ion-col col-3>\n\n        </ion-col>\n      </ion-row>\n      \n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" style="color: #fff;">\n       <img [src]="p.icon" width="20" height="20">\n        <!-- <ion-icon [img]="p.icon" item-left></ion-icon> -->\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[196]);
//# sourceMappingURL=main.js.map