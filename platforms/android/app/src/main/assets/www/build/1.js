webpackJsonp([1],{

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UtcFileRestorePageModule", function() { return UtcFileRestorePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utc_file_restore__ = __webpack_require__(351);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UtcFileRestorePageModule = /** @class */ (function () {
    function UtcFileRestorePageModule() {
    }
    UtcFileRestorePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__utc_file_restore__["a" /* UtcFileRestorePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__utc_file_restore__["a" /* UtcFileRestorePage */]),
            ],
        })
    ], UtcFileRestorePageModule);
    return UtcFileRestorePageModule;
}());

//# sourceMappingURL=utc-file-restore.module.js.map

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtcFileRestorePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the UtcFileRestorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UtcFileRestorePage = /** @class */ (function () {
    function UtcFileRestorePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    UtcFileRestorePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UtcFileRestorePage');
    };
    UtcFileRestorePage.prototype.back = function () {
        this.navCtrl.setRoot('RestorePage');
    };
    UtcFileRestorePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-utc-file-restore',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/utc-file-restore/utc-file-restore.html"*/'\n<ion-content class="Background">\n    <div class="close-btn" (click)=back()>X</div>\n  \n    <ion-row>\n      <ion-col class="info">\n        We\'re going to help you fast as fast as <br>possible.\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4></ion-col>\n        <ion-col col-4>\n            <img src="./assets/imgs/restore_now.png">\n        </ion-col>\n        <ion-col col-4></ion-col>\n      </ion-row>\n  \n    \n    <ion-row>\n        <ion-col class="info1">\n          Now you need to locate your UTC file\n          <br>from where you have downloaded it to. \n          \n        </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col class="info1">\n            Make sure you enter your correct \n            <br>Passphrase when you proceed to\n            <br>Restore.\n          </ion-col>\n        </ion-row>\n\n   \n    <ion-row>\n      <ion-col style="margin: 18PX 25px">\n        <button block ion-button (click)="RestoreNow()">Restore Now</button>\n      </ion-col>\n    </ion-row>\n  \n  \n  </ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/utc-file-restore/utc-file-restore.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], UtcFileRestorePage);
    return UtcFileRestorePage;
}());

//# sourceMappingURL=utc-file-restore.js.map

/***/ })

});
//# sourceMappingURL=1.js.map