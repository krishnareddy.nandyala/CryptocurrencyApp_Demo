webpackJsonp([16],{

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PassphrasePageModule", function() { return PassphrasePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__passphrase__ = __webpack_require__(315);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PassphrasePageModule = /** @class */ (function () {
    function PassphrasePageModule() {
    }
    PassphrasePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__passphrase__["a" /* PassphrasePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__passphrase__["a" /* PassphrasePage */]),
            ],
        })
    ], PassphrasePageModule);
    return PassphrasePageModule;
}());

//# sourceMappingURL=passphrase.module.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PassphrasePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_common_common__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PassphrasePage = /** @class */ (function () {
    function PassphrasePage(navCtrl, navParams, toastCtrl, provider, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.provider = provider;
        this.loadingCtrl = loadingCtrl;
        this.password = this.navParams.get("password");
    }
    PassphrasePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PassphrasePage');
    };
    PassphrasePage.prototype.ionViewDidEnter = function () {
        // let loading = this.loadingCtrl.create({
        //   spinner: 'circles',
        //   content: 'Please wait...'
        // });
        // loading.present();
        // setTimeout(() => {
        //   loading.dismiss();
        // }, 10000)
    };
    PassphrasePage.prototype.presentToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Passphrase Mismatch',
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // toast.onDidDismiss(() => {
    //   console.log('Dismissed toast');
    // });
    PassphrasePage.prototype.back = function () {
        this.navCtrl.setRoot('HomePage');
    };
    PassphrasePage.prototype.submit = function () {
        var loading = this.loadingCtrl.create({
            spinner: 'ios',
            content: 'Please wait...'
        });
        loading.present();
        if (this.Passphrase == this.ConfirmPassphrase) {
            this.provider.generateKey(this.Passphrase);
            loading.dismiss();
            this.navCtrl.setRoot('BackupWalletPage');
        }
        else {
            this.presentToast();
        }
    };
    PassphrasePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-passphrase',template:/*ion-inline-start:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/eth-passphrase/passphrase.html"*/'<ion-content class="Background">\n<div class="close-btn" (click)=back()>X</div>\n\n  <ion-row>\n    <ion-col class="info">\n      Enter Passphrase this is the unique identy of\n      <br> your wallet this will be required at the time of\n      <br> transactions and restoring your wallet.\n    </ion-col>\n  </ion-row>\n\n  <form  #sub="ngForm" novalidate class="form-padding">\n    <ion-row>\n      <ion-col>\n        <ion-input required class="Passphrase" type="text" [(ngModel)]="Passphrase" name="Passphrase" placeholder="Enter Passphrase" \n           minlength=\'4\' ></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-input required class="Passphrase" type="text" [(ngModel)]="ConfirmPassphrase" name="ConfirmPassphrase" placeholder="Conform Passphrase"\n        minlength=\'4\' ></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col style="margin-top: 20PX">\n        <button ion-button [disabled]="sub.invalid" block (click)="submit()">Submit</button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <button block ion-button (click)="back()">Back</button>\n      </ion-col>\n    </ion-row>\n\n\n  </form>\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/CryptocurrencyApp_Demo/src/pages/eth-passphrase/passphrase.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__providers_common_common__["a" /* CommonProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */]])
    ], PassphrasePage);
    return PassphrasePage;
}());

//# sourceMappingURL=passphrase.js.map

/***/ })

});
//# sourceMappingURL=16.js.map