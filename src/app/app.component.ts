import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CardPage } from '../pages/card/card';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { PortfolioPage } from '../pages/portfolio/portfolio';
import { SendReceivePage } from '../pages/send-receive/send-receive';
import { SwapPage } from '../pages/swap/swap';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
 // pages=[];
  @ViewChild(Nav) nav: Nav;

  rootPage: string = 'PasswordPage';

  pages: Array<{title: string, component: any,icon:string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
   this.pages = [
      // { title: 'List', component: ListPage }
      { title: 'Dashboard', component: "DashboardPage",icon:'../assets/icon/home.png',},
      { title: 'Manage Account', component: "DashboardPage" , icon:'../assets/icon/manage.png'},
      { title: 'Porfolio', component: "PortfolioPage" ,icon:'../assets/icon/bag.png'},
      { title: 'Transactions', component: "TransactionsPage" ,icon:'../assets/icon/tranactions.png'},
      { title: 'Wallet', component: 'DashboardPage' ,icon:'../assets/icon/wallet.png'},
      { title: 'Security', component: 'DashboardPage',icon: '../assets/icon/security.png'},

      //{ title: 'Changelog', component: 'DashboardPage',icon:''},
      { title: 'Support', component: 'DashboardPage',icon:'../assets/icon/support.png'},
      { title: 'Logout', component: 'DashboardPage',icon:'../assets/icon/logout.png'},
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
