import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { MyApp } from './app.component';
//import { NgxQRCodeModule } from 'ngx-qrcode2';
// import { ListPage } from '../pages/list/list';
import { CommonProvider } from '../providers/common/common'
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
  import { File } from '@ionic-native/file';
 import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { BitcoinProvider } from '../providers/bitcoin/bitcoin';

// import { IonDigitKeyboard } from '../components/ion-digit-keyboard/ion-digit-keyboard.module';
@NgModule({
  declarations: [
    MyApp,
   
  
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
   // NgxQRCodeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  
  ],
  providers: [
    File,
    CommonProvider,
    SocialSharing,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BitcoinProvider
  ]
})
export class AppModule {}
