import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EnterPrivateKeyPage } from './enter-private-key';

@NgModule({
  declarations: [
    EnterPrivateKeyPage,
  ],
  imports: [
    IonicPageModule.forChild(EnterPrivateKeyPage),
  ],
})
export class EnterPrivateKeyPageModule {}
