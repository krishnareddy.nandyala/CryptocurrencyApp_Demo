import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BackupWalletPage } from './backup-wallet';

@NgModule({
  declarations: [
    BackupWalletPage,
  ],
  imports: [
    IonicPageModule.forChild(BackupWalletPage),
  ],
})
export class BackupWalletPageModule {}
