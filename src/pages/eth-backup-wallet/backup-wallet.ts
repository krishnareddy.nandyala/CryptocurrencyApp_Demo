import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CommonProvider } from '../../providers/common/common'
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Platform } from 'ionic-angular';
//import {File} from '@ionic-native/file/ngx';



@IonicPage()
@Component({
  selector: 'page-backup-wallet',
  templateUrl: 'backup-wallet.html',
})
export class BackupWalletPage {

  ios: any;

  transfer: any;
  storageDirectory: any;
  fileName: string;


  path: any;
  // file: string;
  key: any;
  Passphrase: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public commonProvider: CommonProvider,
    private socialSharing: SocialSharing, public platform: Platform, public file: File) {
    setTimeout(() => {
      this.key = this.commonProvider.privateKey;
      this.file = this.commonProvider.keyFile;
      this.fileName = this.commonProvider.fileName;
    }, 2000)

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BackupWalletPage');
  }
  back() {
    this.navCtrl.setRoot('HomePage')
  }
  // downloadFile() {
  //   this. path = this.file.dataDirectory;
  //   // if (this.platform.is(this.ios)) {
  //   //   path = this.file.documentsDirectory;
  //   // }
  //   // else {
  //   //   path = this.file.dataDirectory;

  //   // }

  //   const transfer = this.transfer.create();

  //   const url = encodeURI(`${this.file}`);
  //    this.fileName = 'img' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.jpeg'
  //   this.storageDirectory = this.file;
  //   transfer.download(url, this.storageDirectory + this.file).then((entry) => {
  //     alert('Image Downloaded')
  //     console.log('success');
  //   }, (error) => {
  //     alert('Error')
  //     console.log('error');
  //   });


  // }


  sharing() {
    //const url = encodeURI(`${this.commonProvider.unEncodedFile}`);
    // console.log(this.commonProvider.unEncodedFile);
    // this.socialSharing.share(null, 'Secured Wallet', null,this.commonProvider.unEncodedFile)
    //   .then((data) => {
        
    //     this.navCtrl.setRoot('YourTokensPage');
    //   }).catch((err) => {
    //     console.log(err);
    //   })
      this.navCtrl.setRoot('YourTokensPage');
  }

}





