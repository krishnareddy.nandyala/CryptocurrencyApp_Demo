import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CommonProvider} from '../../providers/common/common';
import { SocialSharing } from '@ionic-native/social-sharing';
/**
 * Generated class for the PasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-password',
  templateUrl: 'password.html',
})
export class PasswordPage {
  data: string;
  password:any

  constructor(public navCtrl: NavController, private socialSharing: SocialSharing, public navParams: NavParams,private commonPro:CommonProvider) {
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PasswordPage');
  }
  submit(){
    //this.commonProvider.generateKey();
    //this.commonPro.generateKey(this.password)
    this.navCtrl.setRoot('ConfirmPasswordPage',{ 'password': this.password });
  }
  
  //  submit() {
  //   const url = encodeURI(`${this.data}`);
  //   //console.log(this.commonProvider.unEncodedFile);
  //   this.data = "data:text;charset=utf-8,%7B%22address%22%3A%22d9bfe964b1a46d15174b6e749fb3f70fdbe1f5af%22%2C%22crypto%22%3A%7B%22cipher%22%3A%22aes-128-ctr%22%2C%22ciphertext%22%3A%223c3a3c2c285fd2557e28734aa7e586391aefe3fcddb89ea8f218ed7bc824240b%22%2C%22cipherparams%22%3A%7B%22iv%22%3A%22fd9feadb0670c691080c3aeab4182979%22%7D%2C%22mac%22%3A%225a74d24775966cda238f067e56c8a7285ee761070fe8b0fc042fe1d2df052003%22%2C%22kdf%22%3A%22pbkdf2%22%2C%22kdfparams%22%3A%7B%22c%22%3A262144%2C%22dklen%22%3A32%2C%22prf%22%3A%22hmac-sha256%22%2C%22salt%22%3A%22e8fee6ae428fb4681950995e26bf7c27252a71f86f81302c1629daec61deaff8%22%7D%7D%2C%22id%22%3A%2265b9488f-2063-4b78-aff3-6947725f7f7c%22%2C%22version%22%3A3%7D"
  //   this.socialSharing.share(null, 'Secured Wallet', null,this.url)
  //     .then((data) => {
  //       // console.log(this.urls);
  //       this.navCtrl.setRoot('YourTokensPage');
  //     }).catch((err) => {
  //       console.log(err);
  //     })

   //}


  }


