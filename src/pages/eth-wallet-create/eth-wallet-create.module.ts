import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EthWalletCreatePage } from './eth-wallet-create';

@NgModule({
  declarations: [
    EthWalletCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(EthWalletCreatePage),
  ],
})
export class EthWalletCreatePageModule {}
