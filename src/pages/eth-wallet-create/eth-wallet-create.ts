import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EthWalletCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-eth-wallet-create',
  templateUrl: 'eth-wallet-create.html',
})
export class EthWalletCreatePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EthWalletCreatePage');
  }

  addWalletPrivateKey(){
    this.navCtrl.setRoot('EthAddwalletfromprivatekeyPage');
  }
  walletFromJsonfile(){
    this.navCtrl.setRoot('EthImportwalletfromjsonfilePage');
  }
  back() {
    this.navCtrl.setRoot('EthWalletPage')
  }

}
