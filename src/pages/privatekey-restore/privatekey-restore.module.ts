import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrivatekeyRestorePage } from './privatekey-restore';

@NgModule({
  declarations: [
    PrivatekeyRestorePage,
  ],
  imports: [
    IonicPageModule.forChild(PrivatekeyRestorePage),
  ],
})
export class PrivatekeyRestorePageModule {}
