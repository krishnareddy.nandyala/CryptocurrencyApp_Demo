import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PrivatekeyRestorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-privatekey-restore',
  templateUrl: 'privatekey-restore.html',
})
export class PrivatekeyRestorePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrivatekeyRestorePage');
  }
  back(){
    this.navCtrl.setRoot('RestorePage');
  }
  RestoreNow(){
    this.navCtrl.setRoot('EnterPrivateKeyPage');

  }

}
