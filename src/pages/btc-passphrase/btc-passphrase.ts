import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BtcPassphrasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-btc-passphrase',
  templateUrl: 'btc-passphrase.html',
})
export class BtcPassphrasePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BtcPassphrasePage');
  }
  submit(){
    this.navCtrl.setRoot('BtcBackupPage');
  }
  back() {
    this.navCtrl.setRoot('BtcTermsofservicesPage')
  } 
 

}
