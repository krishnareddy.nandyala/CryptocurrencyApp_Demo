import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcPassphrasePage } from './btc-passphrase';

@NgModule({
  declarations: [
    BtcPassphrasePage,
  ],
  imports: [
    IonicPageModule.forChild(BtcPassphrasePage),
  ],
})
export class BtcPassphrasePageModule {}
