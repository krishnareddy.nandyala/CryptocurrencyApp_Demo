import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BtcHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-btc-home',
  templateUrl: 'btc-home.html',
})
export class BtcHomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BtcHomePage');
  }
  forNewWllet(){
    this.navCtrl.setRoot('BtcTermsofservicesPage')
  }
  back() {
    this.navCtrl.setRoot('DashboardPage')
  } 
 
}
