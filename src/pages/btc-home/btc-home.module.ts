import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcHomePage } from './btc-home';

@NgModule({
  declarations: [
    BtcHomePage,
  ],
  imports: [
    IonicPageModule.forChild(BtcHomePage),
  ],
})
export class BtcHomePageModule {}
