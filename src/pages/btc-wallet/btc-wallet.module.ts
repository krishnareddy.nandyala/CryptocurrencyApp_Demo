import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcWalletPage } from './btc-wallet';

@NgModule({
  declarations: [
    BtcWalletPage,
  ],
  imports: [
    IonicPageModule.forChild(BtcWalletPage),
  ],
})
export class BtcWalletPageModule {}
