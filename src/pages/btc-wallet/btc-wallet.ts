import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BtcWalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-btc-wallet',
  templateUrl: 'btc-wallet.html',
})
export class BtcWalletPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BtcWalletPage');
  }

  back() {
    this.navCtrl.setRoot('BtcYourprivatekeyPage')
  } 
  addWallet(){
    this.navCtrl.setRoot('BtcWalletCreatePage');
  }
  walletviewPage(){
    this.navCtrl.setRoot('BtcWalletviewPage');
  }
  
 
}
