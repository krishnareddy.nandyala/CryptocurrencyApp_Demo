import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcWalletCreatePage } from './btc-wallet-create';

@NgModule({
  declarations: [
    BtcWalletCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(BtcWalletCreatePage),
  ],
})
export class BtcWalletCreatePageModule {}
