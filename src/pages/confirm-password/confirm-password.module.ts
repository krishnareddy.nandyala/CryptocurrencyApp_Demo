import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmPasswordPage } from './confirm-password';

@NgModule({
  declarations: [
    ConfirmPasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmPasswordPage),
  ],
})
export class ConfirmPasswordPageModule {}
