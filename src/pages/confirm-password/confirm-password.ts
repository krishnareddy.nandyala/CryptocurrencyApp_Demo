import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

/**
 * Generated class for the ConfirmPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-password',
  templateUrl: 'confirm-password.html',
})
export class ConfirmPasswordPage {

  password: any;
  confirmpassword: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private toastCtrl: ToastController) {
    this.password = this.navParams.get("password")
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmPasswordPage');
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Password Mismatch',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  submit1() {
    if (this.password == this.confirmpassword) {
      this.navCtrl.setRoot('DashboardPage');
    }
    else {
      this.presentToast();
    }

  }



}
