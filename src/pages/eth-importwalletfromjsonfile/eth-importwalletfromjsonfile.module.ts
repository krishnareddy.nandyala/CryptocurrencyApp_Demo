import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EthImportwalletfromjsonfilePage } from './eth-importwalletfromjsonfile';

@NgModule({
  declarations: [
    EthImportwalletfromjsonfilePage,
  ],
  imports: [
    IonicPageModule.forChild(EthImportwalletfromjsonfilePage),
  ],
})
export class EthImportwalletfromjsonfilePageModule {}
