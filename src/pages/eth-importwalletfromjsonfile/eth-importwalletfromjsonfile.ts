import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EthImportwalletfromjsonfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-eth-importwalletfromjsonfile',
  templateUrl: 'eth-importwalletfromjsonfile.html',
})
export class EthImportwalletfromjsonfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EthImportwalletfromjsonfilePage');
  }
  back() {
    this.navCtrl.setRoot('EthWalletCreatePage')
  }
}
