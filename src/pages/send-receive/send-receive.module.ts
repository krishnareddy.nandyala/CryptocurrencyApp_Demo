import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendReceivePage } from './send-receive';

@NgModule({
  declarations: [
    SendReceivePage,
  ],
  imports: [
    IonicPageModule.forChild(SendReceivePage),
  ],
})
export class SendReceivePageModule {}
