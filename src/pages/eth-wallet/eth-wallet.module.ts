import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EthWalletPage } from './eth-wallet';

@NgModule({
  declarations: [
    EthWalletPage,
  ],
  imports: [
    IonicPageModule.forChild(EthWalletPage),
  ],
})
export class EthWalletPageModule {}
