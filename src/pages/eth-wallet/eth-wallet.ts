import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-eth-wallet',
  templateUrl: 'eth-wallet.html',
})
export class EthWalletPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EthWalletPage');
  }

  addWallet(){
    this.navCtrl.setRoot('EthWalletCreatePage');
  }
  back() {
    this.navCtrl.setRoot('YourTokensPage')
  }
  walletviewPage(){
    this.navCtrl.setRoot('EthWalletviewPage');
  }
}
