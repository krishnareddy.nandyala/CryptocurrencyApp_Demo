import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  password: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.password= this.navParams.get("password");
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }


  forNewWllet() {
    this.navCtrl.push('TermsofServicePage' ,{ 'password': this.password })
  }
  restore() {
    this.navCtrl.setRoot('RestorePage')
  }

  back() {
    this.navCtrl.setRoot('DashboardPage')
  }


}
