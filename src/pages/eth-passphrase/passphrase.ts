import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CommonProvider } from '../../providers/common/common';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

@IonicPage()
@Component({
  selector: 'page-passphrase',
  templateUrl: 'passphrase.html',
})
export class PassphrasePage {

  key: any;
  password: any;
  Passphrase: any;
  ConfirmPassphrase: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private toastCtrl: ToastController, public provider: CommonProvider,
    public loadingCtrl: LoadingController) {
    this.password = this.navParams.get("password");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PassphrasePage');
  }
  ionViewDidEnter() {
    // let loading = this.loadingCtrl.create({
    //   spinner: 'circles',
    //   content: 'Please wait...'
    // });
    // loading.present();
    // setTimeout(() => {
    //   loading.dismiss();
    // }, 10000)
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Passphrase Mismatch',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  // toast.onDidDismiss(() => {
  //   console.log('Dismissed toast');
  // });
  

  back() {
    this.navCtrl.setRoot('HomePage')
  }

  submit() {
    let loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: 'Please wait...'
    });
    loading.present();

    if (this.Passphrase == this.ConfirmPassphrase) {
      

      this.provider.generateKey(this.Passphrase);
     
        
      loading.dismiss();
  
      
      this.navCtrl.setRoot('BackupWalletPage')
    
    }
   

    else {
      this.presentToast();
    }
    
  }

}
