import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PassphrasePage } from './passphrase';

@NgModule({
  declarations: [
    PassphrasePage,
  ],
  imports: [
    IonicPageModule.forChild(PassphrasePage),
  ],
})
export class PassphrasePageModule {}
