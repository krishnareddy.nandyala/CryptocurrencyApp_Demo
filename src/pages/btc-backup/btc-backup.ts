import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BtcBackupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-btc-backup',
  templateUrl: 'btc-backup.html',
})
export class BtcBackupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BtcBackupPage');
  }
  sharing(){
    this.navCtrl.setRoot('BtcYourprivatekeyPage')
  }
  back() {
    this.navCtrl.setRoot('BtcPassphrasePage')
  } 
 

}
