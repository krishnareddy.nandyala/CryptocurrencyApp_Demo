import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcBackupPage } from './btc-backup';

@NgModule({
  declarations: [
    BtcBackupPage,
  ],
  imports: [
    IonicPageModule.forChild(BtcBackupPage),
  ],
})
export class BtcBackupPageModule {}
