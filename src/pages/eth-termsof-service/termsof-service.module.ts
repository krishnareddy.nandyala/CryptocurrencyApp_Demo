import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermsofServicePage } from './termsof-service';

@NgModule({
  declarations: [
    TermsofServicePage,
  ],
  imports: [
    IonicPageModule.forChild(TermsofServicePage),
  ],
})
export class TermsofServicePageModule {}
