import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TermsofServicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-termsof-service',
  templateUrl: 'termsof-service.html',
})
export class TermsofServicePage {

  password: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.password= this.navParams.get("password")
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TermsofServicePage');
  }
  openPassphrase(){

    this.navCtrl.setRoot('PassphrasePage', { 'password': this.password });
  }

}
