import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BtcImportwalletfromjsonfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-btc-importwalletfromjsonfile',
  templateUrl: 'btc-importwalletfromjsonfile.html',
})
export class BtcImportwalletfromjsonfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BtcImportwalletfromjsonfilePage');
  }

}
