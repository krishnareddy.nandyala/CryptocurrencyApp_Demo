import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcImportwalletfromjsonfilePage } from './btc-importwalletfromjsonfile';

@NgModule({
  declarations: [
    BtcImportwalletfromjsonfilePage,
  ],
  imports: [
    IonicPageModule.forChild(BtcImportwalletfromjsonfilePage),
  ],
})
export class BtcImportwalletfromjsonfilePageModule {}
