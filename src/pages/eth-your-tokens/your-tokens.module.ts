import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YourTokensPage } from './your-tokens';
import { NgxQRCodeModule } from 'ngx-qrcode2';

@NgModule({
  declarations: [
    YourTokensPage,
  ],
  imports: [
    NgxQRCodeModule,
    IonicPageModule.forChild(YourTokensPage),
  ],
})
export class YourTokensPageModule {}
