import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CommonProvider} from '../../providers/common/common';



@IonicPage()
@Component({
  selector: 'page-your-tokens',
  templateUrl: 'your-tokens.html',
})
export class YourTokensPage {

  prkey: boolean;
  elementType = 'url';
  //value = 'Techiediaries';
  privatekey: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public commonService:CommonProvider) {
    
  }
  
  printscanner(){
    this.prkey = true;
    this.privatekey= this.commonService.privateKey;
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad YourTokensPage');
  }
  goToWallet(){
    this.navCtrl.setRoot('EthWalletPage')
  }

}
