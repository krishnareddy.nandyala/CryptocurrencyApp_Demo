import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-btc-yourprivatekey',
  templateUrl: 'btc-yourprivatekey.html',
})
export class BtcYourprivatekeyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BtcYourprivatekeyPage');
  }
  goToWallet(){
    this.navCtrl.setRoot('BtcWalletPage')
  }
  back() {
    this.navCtrl.setRoot('BtcBackupPage')
  } 
 
}
