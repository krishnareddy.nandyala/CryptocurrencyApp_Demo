import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcYourprivatekeyPage } from './btc-yourprivatekey';

@NgModule({
  declarations: [
    BtcYourprivatekeyPage,
  ],
  imports: [
    IonicPageModule.forChild(BtcYourprivatekeyPage),
  ],
})
export class BtcYourprivatekeyPageModule {}
