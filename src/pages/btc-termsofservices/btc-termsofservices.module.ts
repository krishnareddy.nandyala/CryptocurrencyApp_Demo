import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcTermsofservicesPage } from './btc-termsofservices';

@NgModule({
  declarations: [
    BtcTermsofservicesPage,
  ],
  imports: [
    IonicPageModule.forChild(BtcTermsofservicesPage),
  ],
})
export class BtcTermsofservicesPageModule {}
