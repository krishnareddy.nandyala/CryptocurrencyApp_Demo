import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BtcTermsofservicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-btc-termsofservices',
  templateUrl: 'btc-termsofservices.html',
})
export class BtcTermsofservicesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BtcTermsofservicesPage');
  }
  openPassphrase(){
    this.navCtrl.setRoot('BtcPassphrasePage')
  }

}
