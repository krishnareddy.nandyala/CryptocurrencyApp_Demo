import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEthWalletPage } from './add-eth-wallet';

@NgModule({
  declarations: [
    AddEthWalletPage,
  ],
  imports: [
    IonicPageModule.forChild(AddEthWalletPage),
  ],
})
export class AddEthWalletPageModule {}
