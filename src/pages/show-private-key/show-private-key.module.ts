import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowPrivateKeyPage } from './show-private-key';

@NgModule({
  declarations: [
    ShowPrivateKeyPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowPrivateKeyPage),
  ],
})
export class ShowPrivateKeyPageModule {}
