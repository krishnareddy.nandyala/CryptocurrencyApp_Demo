import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SwapPage } from './swap';

@NgModule({
  declarations: [
    SwapPage,
  ],
  imports: [
    IonicPageModule.forChild(SwapPage),
  ],
})
export class SwapPageModule {}
