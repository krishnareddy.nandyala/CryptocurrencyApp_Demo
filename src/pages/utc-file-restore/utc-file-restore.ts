import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the UtcFileRestorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-utc-file-restore',
  templateUrl: 'utc-file-restore.html',
})
export class UtcFileRestorePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UtcFileRestorePage');
  }
  back(){
    this.navCtrl.setRoot('RestorePage');
  }

}
