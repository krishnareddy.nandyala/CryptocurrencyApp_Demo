import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UtcFileRestorePage } from './utc-file-restore';

@NgModule({
  declarations: [
    UtcFileRestorePage,
  ],
  imports: [
    IonicPageModule.forChild(UtcFileRestorePage),
  ],
})
export class UtcFileRestorePageModule {}
