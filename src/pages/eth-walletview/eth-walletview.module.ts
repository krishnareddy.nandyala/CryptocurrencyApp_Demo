import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EthWalletviewPage } from './eth-walletview';

@NgModule({
  declarations: [
    EthWalletviewPage,
  ],
  imports: [
    IonicPageModule.forChild(EthWalletviewPage),
  ],
})
export class EthWalletviewPageModule {}
