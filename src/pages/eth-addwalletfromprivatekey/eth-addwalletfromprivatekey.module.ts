import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EthAddwalletfromprivatekeyPage } from './eth-addwalletfromprivatekey';

@NgModule({
  declarations: [
    EthAddwalletfromprivatekeyPage,
  ],
  imports: [
    IonicPageModule.forChild(EthAddwalletfromprivatekeyPage),
  ],
})
export class EthAddwalletfromprivatekeyPageModule {}
