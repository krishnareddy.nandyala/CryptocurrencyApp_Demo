import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EthAddwalletfromprivatekeyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-eth-addwalletfromprivatekey',
  templateUrl: 'eth-addwalletfromprivatekey.html',
})
export class EthAddwalletfromprivatekeyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EthAddwalletfromprivatekeyPage');
  }
  back() {
    this.navCtrl.setRoot('EthWalletCreatePage')
  }
}
