import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BtcWalletviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-btc-walletview',
  templateUrl: 'btc-walletview.html',
})
export class BtcWalletviewPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BtcWalletviewPage');
  }

  back() {
    this.navCtrl.setRoot('BtcYourprivatekeyPage')
  } 
}
