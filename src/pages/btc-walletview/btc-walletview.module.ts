import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcWalletviewPage } from './btc-walletview';

@NgModule({
  declarations: [
    BtcWalletviewPage,
  ],
  imports: [
    IonicPageModule.forChild(BtcWalletviewPage),
  ],
})
export class BtcWalletviewPageModule {}
