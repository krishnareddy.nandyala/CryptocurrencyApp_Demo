import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtcAddwalletfromprivatekeyPage } from './btc-addwalletfromprivatekey';

@NgModule({
  declarations: [
    BtcAddwalletfromprivatekeyPage,
  ],
  imports: [
    IonicPageModule.forChild(BtcAddwalletfromprivatekeyPage),
  ],
})
export class BtcAddwalletfromprivatekeyPageModule {}
