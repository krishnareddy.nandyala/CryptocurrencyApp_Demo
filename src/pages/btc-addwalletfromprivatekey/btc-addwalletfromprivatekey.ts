import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BtcAddwalletfromprivatekeyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-btc-addwalletfromprivatekey',
  templateUrl: 'btc-addwalletfromprivatekey.html',
})
export class BtcAddwalletfromprivatekeyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BtcAddwalletfromprivatekeyPage');
  }


  back() {
    this.navCtrl.setRoot('BtcWalletCreatePage');
  } 
  
}
