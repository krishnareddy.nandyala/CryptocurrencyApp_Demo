import { HttpClient } from '@angular/common/http';
import keythereum from 'keythereum'
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Buffer } from 'buffer';
declare var keythereum;
// import {QRCodeComponent} from 'angular2-qrcode'

var params = { keyBytes: 32, ivBytes: 16 };
var kdf = "pbkdf2";
// @Injectable({
//   providedIn: 'root'
// })

@Injectable()
export class CommonProvider {

  // constructor(public http: HttpClient) {
  //   console.log('Hello CommonProvider Provider');
  constructor(private sanitizer: DomSanitizer) { }


  password: string = '';
  privateKey: string = '';
  address: string = '';
  keyObject: any;
  keyFile: any;
  fileName: string = '';
  unEncodedFile:string='';
  // isGenerated: boolean = false;
  // isGenerating: boolean = false;
  //title = 'app';
  // elementType = 'url';
  // value = 'Techiediaries';
  generateKey(Passphrase) {
    // this.isGenerating = true;
    //var password = this.password;
    var kdf = "pbkdf2";
    var dk = keythereum.create();
    var options = {
      kdf: "pbkdf2",
      cipher: "aes-128-ctr",
      kdfparams: {
        c: 262144,
        dklen: 32,
        prf: "hmac-sha256"
      }
    };

    // synchronous
    keythereum.dump(Passphrase, dk.privateKey, dk.salt, dk.iv, options, (cb,err) =>{
      this.keyObject = cb;
      keythereum.exportToFile(this.keyObject);
      console.log(this.keyObject);
      this.address = '0x' + this.keyObject.address;
      this.unEncodedFile ="data:text;charset=utf-8," + encodeURIComponent(JSON.stringify(this.keyObject))
      this.keyFile = this.sanitizer.bypassSecurityTrustUrl("data:text;charset=utf-8," + encodeURIComponent(JSON.stringify(this.keyObject)));
      this.fileName = "UTC--" + (new Date).toISOString() + "--" + this.keyObject.address+".json"
      // this.isGenerated = true;
      // this.isGenerating = false;
      var privateKey = keythereum.recover(Passphrase, this.keyObject);
      console.log(privateKey.toString('hex'));
      this.privateKey = privateKey.toString('hex');
    });



  };
  
}


