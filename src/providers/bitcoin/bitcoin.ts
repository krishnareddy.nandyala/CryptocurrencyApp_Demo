import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BitcoinLib} from 'bitcoinjs-lib';

/*
  Generated class for the BitcoinProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BitcoinProvider {

  address: any;
  wallet: any;
  
  
  network: any;
  walletCache: any;


  constructor(public http: HttpClient) {
    //console.log('Hello BitcoinProvider Provider');
    this.network = BitcoinLib.networks.testnet;

    this.create("kiran").then((x) => {
      console.log(x);
      //this.getBalance(x);
    },
      (err) => {
        console.log(err);
      })
  }

  create(name?: string) {

    return new Promise((resolve, reject) => {
      try {
        let keyPair = BitcoinLib.ECPair.makeRandom({ network: this.network });
        this.walletCache = {
          wif: keyPair.toWIF(),
          address: keyPair.getAddress(),
          name: name ? name : 'My bitcoin wallet'
        };

        resolve(this.walletCache);
      } catch (err) {

        reject(err);
      };
    });
  }
  createWallet() {
    // let loading;
    // this.translate.get('Creating wallet...').subscribe((res: string) => {
    //   loading = this.loadingCtrl.create({
    //     content: res
    //   });
    // });
    // loading.present();
    this.wallet.create().then((wallet) => {
      this.address = wallet['address'];
      // this.walletName = wallet['name'];
      // this.updateBalance();
      // setTimeout(() => {
      //   loading.dismiss();
      // }, 500);
    });
  }

  setWalletName(name: string) {
    this.walletCache.name = name;
    // this.save(this.walletCache);
  }
  
  importWallet(wif: string, name?: string) {
    //this.logger.info('Importing wallet from WIF...');
    let keyPair = BitcoinLib.ECPair.fromWIF(wif, this.network);
    this.walletCache = {
      wif: wif,
      address: keyPair.getAddress(),
      name: name ? name : 'My bitcoin wallet'
    };
    // this.save(this.walletCache);
  }

}
